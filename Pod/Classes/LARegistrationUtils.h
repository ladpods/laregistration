//
//  LARegistrationUtils.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 19/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LARegistrationUtils : NSObject

// ==========================================
/***  METHODS  ***/
// ==========================================

+ (NSString*)sha1:(NSString*)input;

+ (NSString *)base64Encode:(NSString *)plainText;

+ (NSString*)adaptedDateForEu:(NSString*)usDate;

+ (BOOL)mailIsCorrect:(NSString*)mail;

@end
