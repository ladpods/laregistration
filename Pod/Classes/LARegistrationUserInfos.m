//
//  UserInfo.m
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "LARegistrationUserInfos.h"

#import "LARegistrationManager.h"
#import "LARegistrationConstant.h"


static NSString *kUserInfosArchiveFile = @"/laregistrationUserInfos";


@implementation LARegistrationUserInfos

// ---------------------------------------------
#pragma mark - Initialization Methods
// ---------------------------------------------

- (id)init
{
    if (self = [super init])
    {
        [self reinitFields];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveOnDiskUserInfos) name:kLARegistrationNotificationUpdateIsDone object:nil];
    }
    
    return self;    
}

// ---------------------------------------------------------------------------------
#pragma mark - encode & decode méthod

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self)
    {
        self.userNom                  = [aDecoder decodeObjectForKey:@"userNom"];
        self.userPrenom               = [aDecoder decodeObjectForKey:@"userPrenom"];
        self.userBirthday             = [aDecoder decodeObjectForKey:@"userBirthday"];
        self.userId                   = [aDecoder decodeObjectForKey:@"userId"];
        
        self.userAdress               = [aDecoder decodeObjectForKey:@"userAdress"];
        self.userCP                   = [aDecoder decodeObjectForKey:@"userCP"];
        self.userCity                 = [aDecoder decodeObjectForKey:@"userCity"];
        self.userCountry              = [aDecoder decodeObjectForKey:@"userCountry"];
        
        self.userMail                 = [aDecoder decodeObjectForKey:@"userMail"];
        self.userPseudo               = [aDecoder decodeObjectForKey:@"userPseudo"];
        self.userPass                 = [aDecoder decodeObjectForKey:@"userPass"];
        self.userPassConfirmation     = [aDecoder decodeObjectForKey:@"userPassConfirmation"];
        
        self.userToken                = [aDecoder decodeObjectForKey:@"userToken"];
        self.userCredential           = [aDecoder decodeObjectForKey:@"userCredential"];
        
        self.userSex                  = [aDecoder decodeObjectForKey:@"userSex"];
        
        self.newslettersSubscriptions = [aDecoder decodeObjectForKey:@"newslettersSubscriptions"];
        self.tokenAlreadyChecked      = NO;
        self.userInfoBackup           = nil;
    }
    
    return self;
}

// ---------------------------------------------------------------------------------

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.userNom forKey:@"userNom"];
    [encoder encodeObject:self.userPrenom forKey:@"userPrenom"];
    [encoder encodeObject:self.userBirthday forKey:@"userBirthday"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    
    [encoder encodeObject:self.userAdress forKey:@"userAdress"];
    [encoder encodeObject:self.userCP forKey:@"userCP"];
    [encoder encodeObject:self.userCity forKey:@"userCity"];
    [encoder encodeObject:self.userCountry forKey:@"userCountry"];
    
    [encoder encodeObject:self.userMail forKey:@"userMail"];
    [encoder encodeObject:self.userPseudo forKey:@"userPseudo"];
    [encoder encodeObject:self.userPass forKey:@"userPass"];
    [encoder encodeObject:self.userPassConfirmation forKey:@"userPassConfirmation"];
    
    [encoder encodeObject:self.userToken forKey:@"userToken"];
    [encoder encodeObject:self.userCredential forKey:@"userCredential"];
    
    [encoder encodeObject:self.userSex forKey:@"userSex"];
    
    [encoder encodeObject:self.newslettersSubscriptions forKey:@"newslettersSubscriptions"];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// ---------------------------------------------
#pragma mark - Singleton Methods
// ---------------------------------------------

+ (LARegistrationUserInfos *)sharedInstance
{
    static LARegistrationUserInfos *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LARegistrationUserInfos alloc] init];
        
    });
    
    return sharedInstance;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

// ---------------------------------------------
#pragma mark - Data management Methods
// ---------------------------------------------

/**
 * @brief Reset LaRegistrationUserInfos Properties with empty data
 */

- (void)reinitFields
{
    self.userNom                  = @"";
    self.userPrenom               = @"";
    self.userBirthday             = @"";
    self.userId                   = @"";

    self.userAdress               = @"";
    self.userCP                   = @"";
    self.userCity                 = @"";
    self.userCountry              = @"";

    self.userMail                 = @"";
    self.userPseudo               = @"";
    self.userPass                 = @"";
    self.userPassConfirmation     = @"";

    self.userToken                = @"";
    self.userCredential           = @"";

    self.userSex                  = @"";

    self.tokenAlreadyChecked      = NO;
    self.newslettersSubscriptions = [[NSMutableArray alloc] init];
    self.userInfoBackup           = nil;
}

/**
 * @brief Backup all fields value in userInfosBackup Dictionnary
 */

- (void)backupUserInfoDatas
{
    self.userInfoBackup = [[NSMutableDictionary alloc] init];

    [self.userInfoBackup setValue:self.userNom forKey:@"userNom"];
    [self.userInfoBackup setValue:self.userPrenom forKey:@"userPrenom"];
    [self.userInfoBackup setValue:self.userBirthday forKey:@"userBirthday"];
    [self.userInfoBackup setValue:self.userId forKey:@"userId"];

    [self.userInfoBackup setValue:self.userAdress forKey:@"userAdress"];
    [self.userInfoBackup setValue:self.userCP forKey:@"userCP"];
    [self.userInfoBackup setValue:self.userCity forKey:@"userCity"];
    [self.userInfoBackup setValue:self.userCountry forKey:@"userCountry"];

    [self.userInfoBackup setValue:self.userMail forKey:@"userMail"];
    [self.userInfoBackup setValue:self.userPseudo forKey:@"userPseudo"];
    [self.userInfoBackup setValue:self.userPass forKey:@"userPass"];
    [self.userInfoBackup setValue:self.userPassConfirmation forKey:@"userPassConfirmation"];

    [self.userInfoBackup setValue:self.userToken forKey:@"userToken"];
    [self.userInfoBackup setValue:self.userCredential forKey:@"userCredential"];

    [self.userInfoBackup setValue:self.userSex forKey:@"userSex"];

    [self.userInfoBackup setValue:[NSNumber numberWithBool:self.tokenAlreadyChecked] forKey:@"tokenAlreadyChecked"];
    [self.userInfoBackup setValue:self.newslettersSubscriptions forKey:@"newslettersSubscriptions"];
}

/**
 * @brief Reset LaRegistrationUserInfos Properties with backup data
 */

- (void)resetDatasWithBackup
{
    self.userNom = [self.userInfoBackup objectForKey:@"userNom"];
    self.userPrenom = [self.userInfoBackup objectForKey:@"userPrenom"];
    self.userBirthday = [self.userInfoBackup objectForKey:@"userBirthday"];
    self.userId = [self.userInfoBackup objectForKey:@"userId"];
    
    self.userAdress = [self.userInfoBackup objectForKey:@"userAdress"];
    self.userCP = [self.userInfoBackup objectForKey:@"userCP"];
    self.userCity = [self.userInfoBackup objectForKey:@"userCity"];
    self.userCountry = [self.userInfoBackup objectForKey:@"userCountry"];
    
    self.userMail = [self.userInfoBackup objectForKey:@"userMail"];
    self.userPseudo = [self.userInfoBackup objectForKey:@"userPseudo"];
    self.userPass = [self.userInfoBackup objectForKey:@"userPass"];
    self.userPassConfirmation = [self.userInfoBackup objectForKey:@"userPassConfirmation"];
    
    self.userToken = [self.userInfoBackup objectForKey:@"userToken"];
    self.userCredential = [self.userInfoBackup objectForKey:@"userCredential"];
    
    self.userSex = [self.userInfoBackup objectForKey:@"userSex"];
    
    self.tokenAlreadyChecked = [[self.userInfoBackup objectForKey:@"tokenAlreadyChecked"] boolValue];
    self.newslettersSubscriptions = [self.userInfoBackup objectForKey:@"newslettersSubscriptions"];
}

// ---------------------------------------------
#pragma mark - Newsletter Management  Methods
// ---------------------------------------------

/**
 * @brief Get newsletter list from which user has a subscription
 * @return NSMutableArray
 */
- (NSMutableArray*)newslettersUnsubscriptions
{
    NSMutableArray * newslettersArray = [[NSMutableArray alloc] init];
    
    for (NSString * newsletterId in kLARegistrationNewlettersIds)
    {
        if(![self.newslettersSubscriptions containsObject:newsletterId])
        {
            [newslettersArray addObject:newsletterId];
        }
    }
    
    return newslettersArray;
}

/**
 * @brief Add user to a newsletter 
 * @params newsletterId nique identifier for newsletter object
 */
- (void)addNewsletterWithId:(NSString*)newsletterId
{
    if(![self.newslettersSubscriptions containsObject:newsletterId])
    {
        [self.newslettersSubscriptions addObject:newsletterId];
    }
}

/**
 * @brief Unsubscribe user from a specific newsletter identifier
 * @params newsletterId : NewsLetter identifier to unsubscribe
 */
- (void)removeNewsletterWithId:(NSString*)newsletterId
{
    if([self.newslettersSubscriptions containsObject:newsletterId])
    {
        [self.newslettersSubscriptions removeObject:newsletterId];
    }
}

/**
 * @brief Check if user is register for a newsletter identifier
 * @params newsletterId unique identifier for newsletter object
 * @return Bool user is register or not
 */
- (BOOL)newsletterValueForId:(NSString*)newsletterId
{
    return [self.newslettersSubscriptions containsObject:newsletterId];
}

/******************************************************************/
#pragma mark - Serialization méthod
/******************************************************************/

// ---------------------------------------------------------------------------------

- (NSString *)getLARegistrationDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *applicationSupportDirectory = [paths firstObject];
    
    if (applicationSupportDirectory == nil) {
        applicationSupportDirectory = @"";
    }
    
    return applicationSupportDirectory;
}

// ---------------------------------------------------------------------------------

- (BOOL)userInfoArchiveExists
{
    NSString *archivePath     = [[self getLARegistrationDirectoryPath] stringByAppendingString:kUserInfosArchiveFile];
    return [[NSFileManager defaultManager] fileExistsAtPath:archivePath];
}

// ---------------------------------------------------------------------------------

- (BOOL)deleteUserInfoOnDisk
{
    BOOL result = FALSE;
    
    @try
    {
        NSString *archivePath     = [[self getLARegistrationDirectoryPath] stringByAppendingString:kUserInfosArchiveFile];
        NSError *error;
        result = [[NSFileManager defaultManager] removeItemAtPath:archivePath error:&error];
        
        if (error != nil)
        {
            LARLog(@" %@ ",error);
        }
    }
    @catch (NSException *exception)
    {
        LARLog(@"name : %@ | description : %@ ",[exception name],[exception description]);
        result = FALSE;
    }
    @finally
    {
        return  result;
    }
}

// ---------------------------------------------------------------------------------

- (BOOL)saveOnDiskUserInfos
{
    BOOL result = FALSE;
    
    @try
    {
        if (self.userId.length > 0)
        {
            NSString *archivePath     = [[self getLARegistrationDirectoryPath] stringByAppendingString:kUserInfosArchiveFile];
            NSMutableData *data       = [NSMutableData data];
            NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
            
            [archiver encodeObject:self forKey:kUserInfosArchiveFile];
            [archiver finishEncoding];
            
            result                    = [data writeToFile:archivePath atomically:TRUE];
        }
    }
    @catch (NSException *exception)
    {
        LARLog(@"name : %@ | description : %@ ",[exception name],[exception description]);
        result = FALSE;
    }
    @finally
    {
        return result;
    }
}

// ---------------------------------------------------------------------------------

- (void)loadUserInfosFromDisk
{
    @try
    {
        NSString *archivePath     = [[self getLARegistrationDirectoryPath] stringByAppendingString:kUserInfosArchiveFile];
        
        NSData *data = [NSData dataWithContentsOfFile:archivePath];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        
        // Customize the unarchiver.
        typeof(self) backupUserInfo = [unarchiver decodeObjectForKey:kUserInfosArchiveFile];
        [unarchiver finishDecoding];
        
        if (backupUserInfo != nil)
        {
            [self populateWith:backupUserInfo];
        }
    }
    @catch (NSException *exception)
    {
        LARLogException(@"name : %@ | description : %@ ",[exception name],[exception description]);
    }
}

- (void)populateWith:(LARegistrationUserInfos *)backupUserInfo
{
    if (backupUserInfo.userNom.length > 0)
    {
        self.userNom = backupUserInfo.userNom;
    }
    
    if (backupUserInfo.userPrenom.length > 0)
    {
        self.userPrenom = backupUserInfo.userPrenom;
    }
    
    if (backupUserInfo.userBirthday.length > 0)
    {
        self.userBirthday = backupUserInfo.userBirthday;
    }
    
    if (backupUserInfo.userId.length > 0)
    {
        self.userId = backupUserInfo.userId;
    }
    
    if (backupUserInfo.userAdress.length > 0)
    {
        self.userAdress = backupUserInfo.userAdress;
    }
    
    if (backupUserInfo.userCP.length > 0)
    {
        self.userCP = backupUserInfo.userCP;
    }
    
    if (backupUserInfo.userCity.length > 0)
    {
        self.userCity = backupUserInfo.userCity;
    }
    
    if (backupUserInfo.userCountry.length > 0)
    {
        self.userCountry = backupUserInfo.userCountry;
    }
    
    if (backupUserInfo.userMail.length > 0)
    {
        self.userMail = backupUserInfo.userMail;
    }
    
    if (backupUserInfo.userPseudo.length > 0)
    {
        self.userPseudo = backupUserInfo.userPseudo;
    }
    
    if (backupUserInfo.userPass.length > 0)
    {
        self.userPass = backupUserInfo.userPass;
    }
    
    if (backupUserInfo.userPassConfirmation.length > 0)
    {
        self.userPassConfirmation = backupUserInfo.userPassConfirmation;
    }
    
    if (backupUserInfo.userToken.length > 0)
    {
        self.userToken = backupUserInfo.userToken;
    }
    
    if (backupUserInfo.userCredential.length > 0)
    {
        self.userCredential = backupUserInfo.userCredential;
    }
    
    if (backupUserInfo.userSex != nil)
    {
        self.userSex = backupUserInfo.userSex;
    }
    
    if (backupUserInfo.newslettersSubscriptions != nil)
    {
        self.newslettersSubscriptions = backupUserInfo.newslettersSubscriptions;
    }
}


// ---------------------------------------------
#pragma mark - Debug Methods
// ---------------------------------------------

/**
 * @brief Print LARegistrationUserInfos debug values
 */
- (void)debugAllFields
{
#ifdef DEBUG
    LARLog(@"userToken : %@",self.userToken);
    LARLog(@"userCredentials : %@",self.userCredential);
    
    LARLog(@"userPrenom : %@",self.userPrenom);
    LARLog(@"userId : %@",self.userId);
    LARLog(@"userNom : %@",self.userNom);
    LARLog(@"userSex : %@",self.userSex);
    LARLog(@"userMail : %@",self.userMail);
    LARLog(@"userPseudo : %@",self.userPseudo);
    LARLog(@"userPass : %@",self.userPass);
    
    LARLog(@"Adress  : %@",self.userAdress);
    LARLog(@"Newsletters : %@",self.newslettersSubscriptions.description);
#endif
}

/**
 * @brief Override description methods
 */
- (NSString *)description
{
    NSString *superClassDescription = [super description];
    
    NSMutableDictionary *userInfoDetails = [[NSMutableDictionary alloc] init];
    
    [userInfoDetails setValue:self.userNom forKey:@"userNom"];
    [userInfoDetails setValue:self.userPrenom forKey:@"userPrenom"];
    [userInfoDetails setValue:self.userBirthday forKey:@"userBirthday"];
    [userInfoDetails setValue:self.userId forKey:@"userId"];
    
    [userInfoDetails setValue:self.userAdress forKey:@"userAdress"];
    [userInfoDetails setValue:self.userCP forKey:@"userCP"];
    [userInfoDetails setValue:self.userCity forKey:@"userCity"];
    [userInfoDetails setValue:self.userCountry forKey:@"userCountry"];
    
    [userInfoDetails setValue:self.userMail forKey:@"userMail"];
    [userInfoDetails setValue:self.userPseudo forKey:@"userPseudo"];
    [userInfoDetails setValue:self.userPass forKey:@"userPass"];
    [userInfoDetails setValue:self.userPassConfirmation forKey:@"userPassConfirmation"];
    
    [userInfoDetails setValue:self.userToken forKey:@"userToken"];
    [userInfoDetails setValue:self.userCredential forKey:@"userCredential"];
    
    [userInfoDetails setValue:self.userSex forKey:@"userSex"];
    
    [userInfoDetails setValue:[NSNumber numberWithBool:self.tokenAlreadyChecked] forKey:@"tokenAlreadyChecked"];
    [userInfoDetails setValue:self.newslettersSubscriptions forKey:@"newslettersSubscriptions"];
    
    NSString *descriptionTxt = [NSString stringWithFormat:@"%@ \n details : %@",superClassDescription,[userInfoDetails description]];
    
    return descriptionTxt;
}

@end
