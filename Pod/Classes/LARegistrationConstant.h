//
//  LARegistrationConstant.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

static const    NSString    *LAREGISTRATION_SDK_VERSION           = @"0.1.8";

// Logging Methods

#define LARLog(fmt, ...) if([LARegistrationManager sharedInstance].debugLogEnable) NSLog((@"\n\n%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LARLogError(fmt, ...) if([LARegistrationManager sharedInstance].debugLogEnable) NSLog((@"\n\n**** LALogError****\n%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LARLogException(fmt, ...) if([LARegistrationManager sharedInstance].debugLogEnable) NSLog((@"\n\n**** LALogException****\n%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)



#define kHTTPPost                       @"POST"
#define kHTTPGet                        @"GET"
#define kHTTPDelete                     @"DELETE"
#define kHTTPPut                        @"PUT"
#define kHTTPPatch                      @"PATCH"

#define kCredentialUser                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationCredentialUser"]
#define kCredentialPass                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationCredentialPass"]
#define kLARegistrationURLLogin         [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLLogin"]
#define kLARegistrationURLLogout        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLLogout"]
#define kLARegistrationURLRegister      [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLRegister"]
#define kLARegistrationSSOKey           [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationSSOKey"]
#define kLARegistrationSiteId           [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationSiteId"]
#define kLARegistrationURLGetInfo       [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLGetInfo"]
#define kLARegistrationURLSocialLogin   [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLSocialLogin"]
#define kLARegistrationURLNewsletter    [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLNewsletter"]
#define kLARegistrationURLEditInfos     [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLEditInfos"]
#define kLARegistrationURLForgotPass    [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationURLForgotPass"]
#define kLARegistrationNewlettersIds    [[NSBundle mainBundle] objectForInfoDictionaryKey:@"LARegistrationNewlettersIds"]

#define kGPPClientId                    [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GPPIdClient"]

//Api keys
#define kErrorKey                       @"error_key"
#define kInvalidTokenKey                @"invalid_token"
#define kValidationFailedKey            @"validation_failed"
#define kErrorMessageKey                @"error_message"
#define kCrmKey                         @"crm"
#define kFormKey                        @"form"
#define kPlainPasswordKey               @"plainPassword"
#define kFirstKey                       @"first"
#define kEmailKey                       @"email"
#define kUsernameKey                    @"username"
#define kIdKey                          @"id"
#define kGenderKey                      @"gender"
#define kFirstnameKey                   @"firstName"
#define kLastnameKey                    @"lastName"
#define kAddress1Key                    @"address1"
#define kZipCodeKey                     @"zipcode"
#define kCityKey                        @"city"
#define kCountryKey                     @"country"
#define kDisplayUsernameKey             @"display_username"
#define kBirthdayKey                    @"birthdate"
#define kUserKey                        @"user"
#define kTokenKey                       @"token"
#define kNewslettersKey                 @"newsletters"
#define kCurrentPasswordKey             @"current_password"

//Api form keys


//NSUserDefault keys
#define kUserDefaultUserTokenKey        @"userToken"
#define kUserDefaultUserCredentialKey   @"userCredential"
#define kUserDefaultOriginUser          @"originUser"
#define kUserDefaultCurrentUserInfos     @"currentUserInfos"

//Notification
#define kLARegistrationNotificationUserLogged               @"LARegistrationNotificationUserLogged"
#define kLARegistrationNotificationUpdateIsDone             @"LARegistrationNotificationUpdateIsDone"
#define kLARegistrationNotificationUserLogout               @"LARegistrationNotificationUserLogout"
#define kLARegistrationNotificationErrorCreateUser          @"LARegistrationNotificationErrorCreateUser"
#define kLARegistrationNotificationRegisterSuccess          @"LARegistrationNotificationRegisterSuccess"
#define kLARegistrationNotificationDidChangeMail            @"LARegistrationNotificationDidChangeMail"
#define kLARegistrationNotificationDidChangePass            @"LARegistrationNotificationDidChangePass"
#define kLARegistrationNotificationDidSendForgottedPass     @"LARegistrationNotificationDidSendForgottedPass"
#define kLARegistrationNotificationDismissShadowAndPopUp    @"LARegistrationNotificationDismissShadowAndPopUp"
#define kLARegistrationNotificationUpdateIsDone             @"LARegistrationNotificationUpdateIsDone"
