//
//  LARegistrationErrorHandler.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LARegistrationErrorHandler : NSObject

// ==========================================
/***  METHODS  ***/
// ==========================================

+ (LARegistrationErrorHandler*)sharedInstance;

- (NSString*)getErrorWithKey:(NSString*)key;

@end
