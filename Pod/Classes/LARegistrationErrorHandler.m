//
//  LARegistrationErrorHandler.m
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "LARegistrationErrorHandler.h"

@implementation LARegistrationErrorHandler

+ (LARegistrationErrorHandler *)sharedInstance
{
    static LARegistrationErrorHandler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LARegistrationErrorHandler alloc] init];
    });
    
    return sharedInstance;
}

- (NSString*)getErrorWithKey:(NSString*)key
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *bundleUrl     = [mainBundle URLForResource:@"LARegistration" withExtension:@"bundle"];
    NSBundle *bundle     = [NSBundle bundleWithURL:bundleUrl];
   
    NSString *finalPath = [[bundle resourcePath] stringByAppendingString:@"/ErrorKeyAPI.plist"];
    NSMutableDictionary *dict =[ NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
    NSString *value     = [dict objectForKey:key];
    
    return value;
}

@end
