//
//  UserInfo.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LARegistrationUserInfos : NSObject <NSCoding>

/*** PROPERTIES ***/

@property (nonatomic, strong) NSString            *userNom;
@property (nonatomic, strong) NSString            *userPrenom;
@property (nonatomic, strong) NSString            *userBirthday;
@property (nonatomic, strong) NSString            *userId;

@property (nonatomic, strong) NSString            *userAdress;
@property (nonatomic, strong) NSString            *userCP;
@property (nonatomic, strong) NSString            *userCity;
@property (nonatomic, strong) NSString            *userCountry;

@property (nonatomic, strong) NSString            *userSex;
@property (nonatomic, strong) NSString            *userMail;
@property (nonatomic, strong) NSString            *userPseudo;
@property (nonatomic, strong) NSString            *userPass;
@property (nonatomic, strong) NSString            *userPassConfirmation;

@property (nonatomic, strong) NSString            *userToken;
@property (nonatomic, strong) NSString            *userCredential;
@property (nonatomic, strong) NSMutableArray      *newslettersSubscriptions;

@property (nonatomic, strong) NSMutableDictionary *userInfoBackup;

@property (nonatomic, assign) BOOL                tokenAlreadyChecked;

// ==========================================
/***  METHODS  ***/
// ==========================================

#pragma mark - Singleton Methods

+ (LARegistrationUserInfos*)sharedInstance;

// ---------------------------------------------
#pragma mark - Data management Methods
// ---------------------------------------------

// Reset LaRegistrationUserInfos Properties with empty data
- (void)reinitFields;

// Backup all fields value in userInfosBackup Dictionnary
- (void)backupUserInfoDatas;

// Reset LaRegistrationUserInfos Properties with backup data
- (void)resetDatasWithBackup;

// ---------------------------------------------
#pragma mark - Newsletter Management  Methods
// ---------------------------------------------

/**
 * @brief Get newsletter list from which user has a subscription
 * @return NSMutableArray
 */
- (NSMutableArray*)newslettersUnsubscriptions;

/**
 * @brief Add user to a newsletter
 * @params newsletterId nique identifier for newsletter object
 */
- (void)addNewsletterWithId:(NSString*)newsletterId;

/**
 * @brief Unsubscribe user from a specific newsletter identifier
 * @params newsletterId : NewsLetter identifier to unsubscribe
 */
- (void)removeNewsletterWithId:(NSString*)newsletterId;

/**
 * @brief Check if user is register for a newsletter identifier
 * @params newsletterId unique identifier for newsletter object
 * @return Bool user is register or not
 */
- (BOOL)newsletterValueForId:(NSString*)newsletterId;

// ---------------------------------------------
#pragma mark - Serialisation  Methods
// ---------------------------------------------

- (BOOL)userInfoArchiveExists;
- (BOOL)deleteUserInfoOnDisk;
- (BOOL)saveOnDiskUserInfos;
- (void)loadUserInfosFromDisk;

// ---------------------------------------------
#pragma mark - Debug Methods
// ---------------------------------------------


// Print LARegistrationUserInfos debug values
- (void)debugAllFields;

@end
