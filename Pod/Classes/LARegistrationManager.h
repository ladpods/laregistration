//
//  LARegistrationManager.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface LARegistrationManager : NSObject <GIDSignInDelegate>

// ==========================================
/***  PROPERTIES  ***/
// ==========================================

@property (nonatomic, assign) BOOL  shouldShowNewsletterSubscriptionSuccess;
@property (nonatomic, assign) BOOL  debugLogEnable; // Define if debug log are print by default active if DEBUG macro define else unactive
@property (nonatomic, strong) id    userClass;

// ==========================================
/***  METHODS  ***/
// ==========================================

// ---------------------------------------------
#pragma mark - Singleton
// ---------------------------------------------

+ (LARegistrationManager*)sharedInstance;

// ---------------------------------------------
#pragma mark - LARegistration Methods
// ---------------------------------------------

/**
 * @brief Check if already made a login and reconnect automatically
 */
- (void)checkAutoConnect;

/**
 * @brief Logout user from LA Registration API
 */
- (void)logout;

- (void)registerNewUser;

- (NSString*)credentialFromLogin:(NSString*)login andPass:(NSString*)pass;

- (void)loginByUsingCredentials:(NSString*)credentials withSuccess:(void(^)(id response))success failure:(void(^)(id errors))failure;

- (void)logIntoApiWithSocialNetworkWithKey:(NSString*)key Value:(NSString*)idValue andTokenName:(NSString*)tokenName Value:(NSString*)tokenValue withSuccess:(void(^)(id response))success failure:(void(^)(id errors))failure;

// ---------------------------------------------
#pragma mark - Forgot email and pass Methods
// ---------------------------------------------

- (void)changeMail:(NSString*)newMail;

- (void)changePass:(NSString *)lastPass andNewPass:(NSString*)newPass;

- (void)forgotPassWithMailAdress:(NSString*)mailAdress;

// ---------------------------------------------
#pragma mark - Edit NewsLetter API Methods
// ---------------------------------------------

- (void)updateNewslettersSubcription:(NSArray*)nlArray withType:(NSString*)headerType andShowSuccess:(BOOL)showSuccess;

- (void)updateUserInfoWithNewsletterSubscription:(BOOL)shouldUpdateNewsSubscription andNewsletterUnsubscription:(BOOL)shouldUpdateNewsUnsubscription;

// ---------------------------------------------
#pragma mark - Social Login Methods
// ---------------------------------------------

// Check is URL is a callback (deeplink) url for a social sign-in
- (BOOL)isSocialSignInCallbackFor:(NSURL *)aCallbackUrl;

// Try to make social login according  url callback else return FALSE
- (BOOL)socialLoginWith:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation;

// Login on LARegistration API trought Facebook login
- (void)authenticateWithFacebookFromVC:(UIViewController*)controller;

// Login on LARegistration API trought Google SignIn
- (void)authenticateWithGoogle:(GIDSignIn*)signIn user:(GIDGoogleUser*)user withError:(NSError*)error;

// ---------------------------------------------
#pragma mark - Adapt datas for API Methods
// ---------------------------------------------

- (NSMutableDictionary*)dictionnaryToPostWithSocial;

- (NSMutableDictionary*)dictionnaryToCreateNewUser;

- (NSMutableDictionary*)dictionnaryToPostEditUser;

- (void)fillInfoFromApi:(NSDictionary*)user;

- (void)fillUserInfoWithFacebook:(NSDictionary*)user;

// Simple method for check
- (void)debugLogStatus;

@end
