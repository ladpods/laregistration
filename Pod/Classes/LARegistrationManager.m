//
//  LARegistrationManager.m
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 18/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "LARegistrationManager.h"

#import "LARegistrationConstant.h"
#import "LARegistrationErrorHandler.h"
#import "LARegistrationUserInfos.h"
#import "LARegistrationUtils.h"

#import <AFNetworking/AFNetworking.h>

#import <KVNProgress/KVNProgress.h>
#import <TAGManager.h>
#import <TAGDataLayer.h>

#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

// ==========================================
@interface LARegistrationManager ()

@end

// ==========================================

@implementation LARegistrationManager

@synthesize debugLogEnable;

// ==========================================
#pragma mark - Singleton
// ==========================================

+ (LARegistrationManager *)sharedInstance
{
    static LARegistrationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[LARegistrationManager alloc] init];
        
        id userClass = [[NSClassFromString(@"LARegistrationUserInfos") alloc] init];
        sharedInstance.userClass =  [userClass class];
        sharedInstance.debugLogEnable = FALSE;
    });
    
    return sharedInstance;
}

// ==========================================
#pragma mark - LARegistration Methods
// ==========================================

#pragma mark - Auth
/**
 * @brief Check if already made a login and reconnect automatically
 */
- (void)checkAutoConnect
{
    //Check Autoconnect

    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;        
    signIn.clientID = kGPPClientId;
    signIn.scopes = @[ @"profile", @"email" ];
    signIn.delegate = self;

    [[self.userClass sharedInstance]setTokenAlreadyChecked:YES];
    
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"network":@""}];
    
    //cas deja logué
    if ([[[self.userClass sharedInstance]userToken]length] != 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationUserLogged object:nil];
        
        return;
    }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserTokenKey]length])
    {
        //on test la validité du token
        [self getUserInfosForTokenUSer:[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserTokenKey] autoLogin:true];
    }
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserCredentialKey]length])
    {
        // pas de token on test le login/password
        [self loginByUsingCredentials:[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserCredentialKey] autoLogin:true withSuccess:nil failure:nil];
    }
    else if([[GIDSignIn sharedInstance] hasAuthInKeychain])
    {
        //si token Google+ on le revalide.
        [[GIDSignIn sharedInstance] signInSilently];
    }
    
    //pour FB rien a faire
}

// ==========================================
#pragma mark - LARegistration : Login Methods
// ==========================================

- (void)loginByUsingCredentials:(NSString*)credentials withSuccess:(void(^)(id response))success failure:(void(^)(id errors))failure
{
    [self loginByUsingCredentials:credentials autoLogin:false withSuccess:success failure:failure];
}

- (void)loginByUsingCredentials:(NSString*)credentials autoLogin:(BOOL) isAutoLogin withSuccess:(void(^)(id response))success failure:(void(^)(id errors))failure

{
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"network":@"interne"}];
    
    if([kLARegistrationURLLogin length])
    {
        NSDictionary * cred = [[NSDictionary alloc]initWithObjectsAndKeys:credentials,@"credentials",nil];
        
        NSError * error;
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPost URLString:kLARegistrationURLLogin   parameters:cred error:&error];
        [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
        
        LARLog(@"LOG INFO : User = %@, Pass = %@, URL = %@",kCredentialUser,kCredentialPass,kLARegistrationURLLogin);
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error == nil)
            {
                
                NSDictionary *response =responseObject;
                
                if(response)
                {
                    if ([[response objectForKey:kUserKey]objectForKey:kErrorKey])
                    {
                        if(failure != nil)
                        {
                            failure(response);
                        }
                        else
                        {
                            [KVNProgress dismiss];
                            [self forceKVNDismiss];

                            [self catchAndDisplayError:responseObject];
                            
                            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                            [dataLayer push:@{@"event": @"sendUserInfo",
                                              @"login":@"",
                                              @"email":@"",
                                              @"userid":@"0",
                                              @"firstName":@"",
                                              @"lastName":@"",
                                              @"gender":@"",
                                              @"birthdate":@"",
                                              @"zipcode":@"",
                                              @"city":@"",
                                              @"country":@""}];
                            
                            if(isAutoLogin)
                            {
                                //login/password faux
                                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserDefaultUserCredentialKey];
                                [self checkAutoConnect];
                                return;
                            }
                            else
                            {
                                [dataLayer push:@{@"event": @"sendEvent",
                                                  @"eventName":@"Compte",
                                                  @"action": @"Erreur",
                                                  @"section": @"",
                                                  @"type": @"",
                                                  @"title":@""}];
                            }
                            
                            if([kLARegistrationSiteId length])
                            {
                                [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"false",@"siteId":kLARegistrationSiteId}];
                            }
                            else
                            {
                                LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
                            }
                        }
                    }
                    else
                    {
                        if(success != nil)
                        {
                            success(response);
                        }
                        else
                        {
                            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                            
                            LARLog(@"User token %@",[[response objectForKey:kUserKey]objectForKey:kTokenKey]);
                            
                            [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:kUserKey] objectForKey:kTokenKey] forKey:kUserDefaultUserTokenKey];
                            
                            NSDictionary * user=[response objectForKey:kUserKey];
                            [self loadInfoFromUserDictionary:user];
                            NSDictionary *userDetail=[user valueForKey:@"crm"];
                            
                            [dataLayer push:@{
                                              @"login":[user valueForKey:@"username"],
                                              @"email":[user valueForKey:@"email"],
                                              @"userid":[user valueForKey:@"id"]
                                              }];
                            
                            if(userDetail!=nil)
                            {
                                [dataLayer push:@{
                                                  @"firstName":[userDetail valueForKey:@"firstName"]==nil?@"":[userDetail valueForKey:@"firstName"],
                                                  @"lastName":[userDetail valueForKey:@"lastName"]==nil?@"":[userDetail valueForKey:@"lastName"],
                                                  @"gender":[userDetail valueForKey:@"gender"]==nil?@"":[userDetail valueForKey:@"gender"],
                                                  @"birthdate":[userDetail valueForKey:@"birthdate"]==nil?@"":[userDetail valueForKey:@"birthdate"],
                                                  @"zipcode":[userDetail valueForKey:@"zipcode"]==nil?@"":[userDetail valueForKey:@"zipcode"],
                                                  @"city":[userDetail valueForKey:@"city"]==nil?@"":[userDetail valueForKey:@"city"],
                                                  @"country":[userDetail valueForKey:@"country"]==nil?@"":[userDetail valueForKey:@"country"]
                                                  }];
                            }
                            
                            [dataLayer push:@{@"event": @"sendUserInfo"}];
                            
                            if(!isAutoLogin)
                            {
                                [dataLayer push:@{@"event": @"sendEvent",
                                                  @"eventName":@"Compte",
                                                  @"action": @"login",
                                                  @"section": @"",
                                                  @"type": @"",
                                                  @"title":@""}];
                            }
                            
                            if([kLARegistrationSiteId length])
                            {
                                [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"true",@"siteId":kLARegistrationSiteId}];
                            }
                            else
                            {
                                LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
                            }
                            
                            [KVNProgress dismiss];
                            [self forceKVNDismiss];
                        }
                    }
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationUpdateIsDone object:nil];
            }
            else
            {
                
                if(failure != nil)
                {
                    failure(response);
                }
                else
                {
                    [KVNProgress dismiss];
                    [self forceKVNDismiss];
                    
                    [self catchAndDisplayError:responseObject];
                    
                    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                    [dataLayer push:@{@"event": @"sendUserInfo",
                                      @"login":@"",
                                      @"email":@"",
                                      @"userid":@"0",
                                      @"firstName":@"",
                                      @"lastName":@"",
                                      @"gender":@"",
                                      @"birthdate":@"",
                                      @"zipcode":@"",
                                      @"city":@"",
                                      @"country":@""}];
                    
                    [dataLayer push:@{@"event": @"sendEvent",
                                      @"eventName":@"Compte",
                                      @"action": @"Erreur",
                                      @"section": @"",
                                      @"type": @"",
                                      @"title":@""}];
                    
                    if([kLARegistrationSiteId length])
                    {
                        [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"false",@"siteId":kLARegistrationSiteId}];
                    }
                    else
                    {
                        LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
                    }
                }

            }
        }];
        
        [dataTask resume];
    }
    else
    {
        LARLogError(@"%@ : LARegistrationURLLogin required in plist info",[LARegistrationManager class]);
    }
}

- (void)logIntoApiWithSocialNetworkWithKey:(NSString*)key Value:(NSString*)idValue andTokenName:(NSString*)tokenName Value:(NSString*)tokenValue withSuccess:(void(^)(id response))success failure:(void(^)(id errors))failure;
{
    @try
    {
        NSMutableDictionary * params = [self dictionnaryToPostWithSocial];
        
        [params addEntriesFromDictionary:@{
                                           key:idValue,
                                           tokenName:tokenValue
                                           }];
        
        NSError * error;
      
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPost URLString:kLARegistrationURLSocialLogin parameters:params error:&error];
        [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
            if(error == nil)
            {
                NSDictionary *response =responseObject;
    
                if(success != nil)
                {
                    success(response);
                }
                else
                {
                    NSDictionary * user = [response objectForKey:kUserKey];
    
                    [self loadInfoFromUserDictionary:user];
    
                    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                    [dataLayer push:@{
                                      @"login":[user valueForKey:@"username"],
                                      @"email":[user valueForKey:@"email"],
                                      @"userid":[user valueForKey:@"id"]
                                      }];
    
                    NSDictionary *userDetail=[user valueForKey:@"crm"];
    
                    if(userDetail!=nil)
                    {
                        [dataLayer push:@{
                                          @"firstName":[userDetail valueForKey:@"firstName"]==nil?@"":[userDetail valueForKey:@"firstName"],
                                          @"lastName":[userDetail valueForKey:@"lastName"]==nil?@"":[userDetail valueForKey:@"lastName"],
                                          @"gender":[userDetail valueForKey:@"gender"]==nil?@"":[userDetail valueForKey:@"gender"],
                                          @"birthdate":[userDetail valueForKey:@"birthdate"]==nil?@"":[userDetail valueForKey:@"birthdate"],
                                          @"zipcode":[userDetail valueForKey:@"zipcode"]==nil?@"":[userDetail valueForKey:@"zipcode"],
                                          @"city":[userDetail valueForKey:@"city"]==nil?@"":[userDetail valueForKey:@"city"],
                                          @"country":[userDetail valueForKey:@"country"]==nil?@"":[userDetail valueForKey:@"country"]                                      }];
                    }
    
                    [dataLayer push:@{@"event": @"sendUserInfo"}];
                    [dataLayer push:@{@"event": @"sendEvent",
                                      @"eventName":@"Compte",
                                      @"action": @"login",
                                      @"section": @"",
                                      @"type": @"",
                                      @"title":@""}];
    
                    if([kLARegistrationSiteId length])
                    {
                        [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"true",@"siteId":@"5"}];
    
                        [KVNProgress dismiss];
                        [self forceKVNDismiss];
                    }
                    else
                    {
                        LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
                    }
                }           
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationUpdateIsDone object:nil];
            }
            else
            {
                LARLogError(@" => Error: %@ AND description %@", error, [error description]);
    
                if(failure != nil)
                {
                    failure(response);
                }
                else
                {
                    [KVNProgress dismiss];
                    [self forceKVNDismiss];
                    [self catchAndDisplayError:responseObject];
    
                    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                    [dataLayer push:@{@"event": @"sendUserInfo",
                                      @"login":@"",
                                      @"email":@"",
                                      @"userid":@"0",
                                      @"firstName":@"",
                                      @"lastName":@"",
                                      @"gender":@"",
                                      @"birthdate":@"",
                                      @"zipcode":@"",
                                      @"city":@"",
                                      @"country":@""}];
    
                    [dataLayer push:@{@"event": @"sendEvent",
                                      @"eventName":@"Compte",
                                      @"action": @"Erreur",
                                      @"section": @"",
                                      @"type": @"",
                                      @"title":@""}];
    
                    if([kLARegistrationSiteId length])
                    {
                        [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"false",@"siteId":kLARegistrationSiteId}];
                    }
                    else
                    {
                        LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
                    }
                }
            }
            
        }];
        
        [dataTask resume];
    }
    @catch (NSException *exception)
    {
        LARLogException(@"Imposible de se connecter %@",[exception reason]);
        
        [KVNProgress dismiss];
        [self forceKVNDismiss];
    }
}

- (NSString*)credentialFromLogin:(NSString*)login andPass:(NSString*)pass
{
    NSString *userCredential = [NSString stringWithFormat:@"%@:%@",login, pass];
    return [LARegistrationUtils base64Encode:userCredential];
}

/**
 * @brief Logout user from LA Registration API
 */
- (void)logout
{
    [[GIDSignIn sharedInstance] signOut];
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    NSString *tokenOriginal = [[self.userClass sharedInstance]userToken];
    NSString *tokenSha1 = [LARegistrationUtils sha1:tokenOriginal];
    NSString *ssoKeySha1 = [LARegistrationUtils sha1:kLARegistrationSSOKey];
    NSString *keyAndTokenOriginal = [NSString stringWithFormat:@"%@-%@",ssoKeySha1,tokenSha1];
    NSString *firstSha1KeyAndToken = [LARegistrationUtils sha1:keyAndTokenOriginal];
    NSString * logoutAppen = [NSString stringWithFormat:@"%@%@",kLARegistrationURLLogout, firstSha1KeyAndToken];
    
    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPost URLString:logoutAppen   parameters:nil error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-type"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if(error == nil)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationUserLogout object:nil];
    
            [[self.userClass sharedInstance] deleteUserInfoOnDisk];
    
            LARLog(@"Response Logout %@", responseObject);
        }
        else
        {
             LARLogError(@"Failure: %@", error);
        }
    }];
    
    [dataTask resume];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserDefaultUserTokenKey];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:kUserDefaultUserCredentialKey];
    [[self.userClass sharedInstance]setUserToken:@""];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:kUserDefaultOriginUser];
    
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"sendUserInfo",
                      @"login":@"",
                      @"email":@"",
                      @"userid":@"0",
                      @"firstName":@"",
                      @"lastName":@"",
                      @"gender":@"",
                      @"birthdate":@"",
                      @"zipcode":@"",
                      @"city":@"",
                      @"country":@""}];
    
    [dataLayer push:@{@"event": @"sendEvent",
                      @"eventName":@"Compte",
                      @"action": @"logout",
                      @"section": @"",
                      @"type": @"",
                      @"title":@"",
                      @"network":@""}];
    
    if([kLARegistrationSiteId length])
    {
        [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"false",@"siteId":kLARegistrationSiteId}];
    }
    else
    {
        LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
    }
}

- (void)getUserInfosForTokenUSer:(NSString*)tokenUser autoLogin:(BOOL)isAutoLogin
{
    NSString *tokenOriginal = tokenUser;
    NSString *tokenSha1 = [LARegistrationUtils sha1:tokenOriginal];
    NSString *ssoKeySha1 = [LARegistrationUtils sha1:kLARegistrationSSOKey];
    NSString *keyAndTokenOriginal = [NSString stringWithFormat:@"%@-%@",ssoKeySha1,tokenSha1];
    NSString *firstSha1KeyAndToken = [LARegistrationUtils sha1:keyAndTokenOriginal];
    NSString * urlWithToken = [NSString stringWithFormat:@"%@%@",kLARegistrationURLGetInfo,firstSha1KeyAndToken];

    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPGet URLString:urlWithToken parameters:nil error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if(error == nil)
        {
            NSDictionary *reponse =responseObject;
    
            NSDictionary * userAnswer  = [reponse objectForKey:kUserKey] ;
    
            LARLog(@"Response for GetUserAnswer : %@", userAnswer);
    
            if(userAnswer)
            {
                if ([userAnswer objectForKey:kErrorKey])
                {
                    LARLog(@"Error token invalide : %@", userAnswer);
    
                    [[self.userClass sharedInstance]setUserToken:@""];
                    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserDefaultUserTokenKey];
                    [self checkAutoConnect];
                }
                else
                {
                    LARLog(@"Token %@", [responseObject allKeys]);
    
                    [self loadInfoFromUserDictionary:userAnswer];
                    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                    [dataLayer push:@{
                                      @"login":[userAnswer valueForKey:@"username"],
                                      @"email":[userAnswer valueForKey:@"email"],
                                      @"userid":[userAnswer valueForKey:@"id"]
                                      }];
    
                    NSDictionary *userDetail=[userAnswer valueForKey:@"crm"];
    
                    if(userDetail!=nil)
                    {
                        [dataLayer push:@{
                                          @"firstName":[userDetail valueForKey:@"firstName"]==nil?@"":[userDetail valueForKey:@"firstName"],
                                          @"lastName":[userDetail valueForKey:@"lastName"]==nil?@"":[userDetail valueForKey:@"lastName"],
                                          @"gender":[userDetail valueForKey:@"gender"]==nil?@"":[userDetail valueForKey:@"gender"],
                                          @"birthdate":[userDetail valueForKey:@"birthdate"]==nil?@"":[userDetail valueForKey:@"birthdate"],
                                          @"zipcode":[userDetail valueForKey:@"zipcode"]==nil?@"":[userDetail valueForKey:@"zipcode"],
                                          @"city":[userDetail valueForKey:@"city"]==nil?@"":[userDetail valueForKey:@"city"],
                                          @"country":[userDetail valueForKey:@"country"]==nil?@"":[userDetail valueForKey:@"country"]
                                          }];
                    }
    
                    [dataLayer push:@{@"event": @"sendUserInfo"}];
    
                    if(!isAutoLogin)
                    {
                        [dataLayer push:@{@"event": @"sendEvent",
                                          @"eventName":@"Compte",
                                          @"action": @"login",
                                          @"section": @"",
                                          @"type": @"",
                                          @"title":@"",
                                          @"network":@""}];
                    }
    
                    if([kLARegistrationSiteId length])
                    {
                        TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                        [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"true",@"siteId":kLARegistrationSiteId}];
                    }
                    else
                    {
                        LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
                    }
                }
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationUpdateIsDone object:nil];
        }
        else
        {
            [self catchAndDisplayError:responseObject];
    
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"event": @"sendUserInfo",
                              @"login":@"",
                              @"email":@"",
                              @"userid":@"0",
                              @"firstName":@"",
                              @"lastName":@"",
                              @"gender":@"",
                              @"birthdate":@"",
                              @"zipcode":@"",
                              @"city":@"",
                              @"country":@""}];
    
            if([kLARegistrationSiteId length])
            {
                [dataLayer push:@{@"event": @"appInfo", @"rubrique": @"Identification",@"isLog":@"false",@"siteId":kLARegistrationSiteId}];
            }
            else
            {
                LARLogError(@"%@ : kLARegistrationSiteId required in plist info for TagManager",[LARegistrationManager class]);
            }
            
            LARLogError(@"Error: %@", [error localizedDescription]);

        }
    }];
    
    [dataTask resume];
}

- (void)registerNewUser
{
    NSMutableDictionary * params = [self dictionnaryToCreateNewUser];
    
    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPost URLString:kLARegistrationURLRegister   parameters:params error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error == nil)
        {
            NSDictionary *reponse = responseObject;
    
            if ([[reponse objectForKey:kUserKey] objectForKey:kErrorKey])
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationErrorCreateUser object:responseObject];
                [KVNProgress dismiss];
                [self forceKVNDismiss];
                return ;
            }
    
            [self updateNewslettersSubcription:[[self.userClass sharedInstance] newslettersSubscriptions] withType:kHTTPPost andShowSuccess:self.shouldShowNewsletterSubscriptionSuccess];
    
            NSDictionary * userAnswer  = [reponse objectForKey:kUserKey] ;
    
            LARLog(@"__REGISTER USER ANSWER : %@",userAnswer);
    
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{
                              @"login":[userAnswer valueForKey:@"username"],
                              @"email":[userAnswer valueForKey:@"email"],
                              @"userid":[userAnswer valueForKey:@"id"]
                              }];
    
            NSDictionary *userDetail=[userAnswer valueForKey:@"crm"];
    
            if(userDetail!=nil)
            {
                [dataLayer push:@{
                                  @"firstName":[userDetail valueForKey:@"firstName"]==nil?@"":[userDetail valueForKey:@"firstName"],
                                  @"lastName":[userDetail valueForKey:@"lastName"]==nil?@"":[userDetail valueForKey:@"lastName"],
                                  @"gender":[userDetail valueForKey:@"gender"]==nil?@"":[userDetail valueForKey:@"gender"],
                                  @"birthdate":[userDetail valueForKey:@"birthdate"]==nil?@"":[userDetail valueForKey:@"birthdate"],
                                  @"zipcode":[userDetail valueForKey:@"zipcode"]==nil?@"":[userDetail valueForKey:@"zipcode"],
                                  @"city":[userDetail valueForKey:@"city"]==nil?@"":[userDetail valueForKey:@"city"],
                                  @"country":[userDetail valueForKey:@"country"]==nil?@"":[userDetail valueForKey:@"country"]
                                  }];
            }
    
            [dataLayer push:@{@"event": @"sendUserInfo"}];
            [dataLayer push:@{@"event": @"sendEvent",
                              @"eventName":@"Compte",
                              @"action": @"Creation",
                              @"section": @"",
                              @"type": @"",
                              @"title":@""}];
            
            NSString * userCredential = [NSString stringWithFormat:@"%@:%@",[[self.userClass sharedInstance] userMail], [[self.userClass sharedInstance] userPass]];
            
            userCredential = [LARegistrationUtils base64Encode:userCredential];
            [[NSUserDefaults standardUserDefaults]setObject:userCredential forKey:kUserDefaultUserCredentialKey];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationRegisterSuccess object:nil];

        }
        else
        {
            [self catchAndDisplayError:responseObject];
        }
        
    }];
    
    [dataTask resume];
}

// ==========================================
#pragma mark - Forgot email and pass Methods
// ==========================================

- (void)changeMail:(NSString*)newMail
{
    NSString *tokenOriginal = [[self.userClass sharedInstance]userToken];
    LARLog(@"Token original %@",tokenOriginal);
    
    NSString *tokenSha1 = [LARegistrationUtils sha1:tokenOriginal];
    LARLog(@"Token Sha1 %@",tokenSha1);
    
    NSString *ssoKeySha1 = [LARegistrationUtils sha1:kLARegistrationSSOKey];
    
    NSString *keyAndTokenOriginal = [NSString stringWithFormat:@"%@-%@",ssoKeySha1,tokenSha1];
    LARLog(@"keyAndTokenOriginal Sha1 %@",keyAndTokenOriginal);
    
    NSString *firstSha1KeyAndToken = [LARegistrationUtils sha1:keyAndTokenOriginal];
    LARLog(@"firstSha1KeyAndToken Sha1 %@",firstSha1KeyAndToken);
    
    NSString * changeMailUrl = [NSString stringWithFormat:@"%@%@/email",kLARegistrationURLEditInfos,firstSha1KeyAndToken];
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
    [params addEntriesFromDictionary:@{
                                       @"la_user_change_email[email]": newMail
                                       }];
    
    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPatch URLString:changeMailUrl   parameters:params error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-type"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error == nil)
        {
            //NSDictionary * user  =responseObject;
    
            LARLog(@"Response Update change Mail %@", responseObject);
    
            if ([[responseObject objectForKey:kUserKey]objectForKey:kErrorKey])
            {
                NSString * errorToDisplay = [[LARegistrationErrorHandler sharedInstance] getErrorWithKey:[[responseObject objectForKey:kUserKey]objectForKey:kErrorKey]];
                [KVNProgress showErrorWithStatus:errorToDisplay];
            }
            else
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationDismissShadowAndPopUp object:self];
                [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationDidChangeMail object:self];
            }
        }
        else
        {
            LARLogError(@" => Error: %@", [error localizedDescription]);
            [self catchAndDisplayError:responseObject];
            //[KVNProgress showErrorWithStatus:@"Error"];
        }
    }];
    
    [dataTask resume];
}

- (void)changePass:(NSString *)lastPass andNewPass:(NSString*)newPass
{
    NSString *tokenOriginal = [[self.userClass sharedInstance]userToken];
    LARLog(@"Token original %@",tokenOriginal);
    
    NSString *tokenSha1 = [LARegistrationUtils sha1:tokenOriginal];
    LARLog(@"Token Sha1 %@",tokenSha1);
    
    NSString *ssoKeySha1 = [LARegistrationUtils sha1:kLARegistrationSSOKey];
    
    NSString *keyAndTokenOriginal = [NSString stringWithFormat:@"%@-%@",ssoKeySha1,tokenSha1];
    LARLog(@"keyAndTokenOriginal Sha1 %@",keyAndTokenOriginal);
    
    NSString *firstSha1KeyAndToken = [LARegistrationUtils sha1:keyAndTokenOriginal];
    LARLog(@"firstSha1KeyAndToken Sha1 %@",firstSha1KeyAndToken);
    
    NSString *changePassUrl = [NSString stringWithFormat:@"%@%@/password",kLARegistrationURLEditInfos,firstSha1KeyAndToken];
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
    [params addEntriesFromDictionary:@{
                                       @"la_user_change_password[current_password]": lastPass,
                                       @"la_user_change_password[plainPassword][first]": newPass,
                                       @" la_user_change_password[plainPassword][second]": newPass,
                                       }];
    
    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPatch URLString:changePassUrl   parameters:params error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-type"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error == nil)
        {
            //NSDictionary * user  =responseObject;
    
            LARLog(@"Response Update changePass %@", responseObject);
    
            if ([[[responseObject objectForKey:kUserKey]allKeys]containsObject:kFormKey])
            {
                if ([[[[responseObject objectForKey:kUserKey] objectForKey:kFormKey]allKeys]containsObject:kCurrentPasswordKey])
                {
                    NSString * keyError = [[[[responseObject objectForKey:kUserKey] objectForKey:kFormKey] objectForKey:kCurrentPasswordKey] objectForKey:kErrorMessageKey];
    
                    [KVNProgress showErrorWithStatus:keyError];
                    return ;
                }
            }
    
            if ([[responseObject objectForKey:kUserKey] objectForKey:kErrorKey])
            {
                [KVNProgress showErrorWithStatus:[[responseObject objectForKey:kUserKey]objectForKey:kErrorKey]];
            }
            else
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationDismissShadowAndPopUp object:self];
                [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationDidChangePass object:self];
                [KVNProgress showSuccess];
            }

        }
        else
        {
            LARLogError(@"Error : %@", [error localizedDescription]);
            [self catchAndDisplayError:responseObject];
            //[KVNProgress showErrorWithStatus:@"Error"];
        }
    }];
    
    [dataTask resume];
}

- (void)forgotPassWithMailAdress:(NSString*)mailAdress
{
    NSString * logoutAppen = [NSString stringWithFormat:@"%@%@",kLARegistrationURLForgotPass,mailAdress];
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
    [params addEntriesFromDictionary:@{
                                       @"email": mailAdress
                                       }];
    
    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPost URLString:logoutAppen   parameters:params error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-type"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error == nil)
        {
            //NSDictionary *user  =responseObject;
    
            LARLog(@"Forgot Pass Response Update %@", responseObject);
    
            if ([[responseObject objectForKey:kUserKey] objectForKey:kErrorKey])
            {
                NSString * errorToDisplay = [[LARegistrationErrorHandler sharedInstance] getErrorWithKey:[[responseObject objectForKey:kUserKey] objectForKey:kErrorKey]];
                [KVNProgress showErrorWithStatus:errorToDisplay];
            }
            else
            {   [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationDismissShadowAndPopUp object:self];
                [[NSNotificationCenter defaultCenter]postNotificationName:kLARegistrationNotificationDidSendForgottedPass object:self];
            }
        }
        else
        {
            LARLogError(@" => Error: %@", [error localizedDescription]);
            [self catchAndDisplayError:responseObject];
            //[KVNProgress showErrorWithStatus:@"Error"];
        }
    }];
    
    [dataTask resume];
}

// ==========================================
#pragma mark - Edit NewsLetter API Methods
// ==========================================

- (void)updateNewslettersSubcription:(NSArray*)nlArray withType:(NSString*)headerType andShowSuccess:(BOOL)showSuccess
{
    if(kLARegistrationNewlettersIds)
    {
        if([nlArray count] > 0)
        {
            NSString * editNews = [NSString stringWithFormat:@"%@%@",kLARegistrationURLNewsletter, [[self.userClass sharedInstance]userMail]];
            
            NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
            
            [params addEntriesFromDictionary:@{
                                               @"newsletters":nlArray
                                               }];
            
            NSError * error;
            
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            [manager.requestSerializer setHTTPMethodsEncodingParametersInURI:[NSSet setWithArray:@[]]];
            
            NSMutableURLRequest * request = [manager.requestSerializer requestWithMethod:headerType URLString:editNews parameters:params error:&error];
            [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
            [request setValue: @"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-type"];
            
            NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if (error == nil)
                {
                    if(showSuccess)
                    {
                        [KVNProgress showSuccess];
                    }
                    
                    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                    
                    for (NSString * newsletterId in kLARegistrationNewlettersIds)
                    {
                        for (NSString * idNews in nlArray)
                        {
                            if([idNews isEqualToString:newsletterId])
                            {
                                if([headerType isEqualToString:kHTTPDelete])
                                {
                                    [[self.userClass sharedInstance] removeNewsletterWithId:newsletterId];
                                    [dataLayer push:@{@"event": @"sendNewsletter",
                                                      @"newsletterId":newsletterId,
                                                      @"status":@false}];
                                }
                                else
                                {
                                    [[self.userClass sharedInstance] addNewsletterWithId:newsletterId];
                                    [dataLayer push:@{@"event": @"sendNewsletter",
                                                      @"newsletterId":newsletterId,
                                                      @"status":@true}];
                                }
                            }
                        }
                    }
                }
                else
                {
                    [KVNProgress showErrorWithStatus:[[LARegistrationErrorHandler sharedInstance] getErrorWithKey:@"unkwown_error"]];
                    //[KVNProgress showErrorWithStatus:@"Error"];
                    
                    LARLogError(@" => Failure: %@", [error localizedDescription]);
                }
            }];
            
            [dataTask resume];
        }
        else
        {
            LARLogError(@"%@ :  newsletters subcription is empty",[LARegistrationManager class]);
        }


    }
    else
    {
         LARLogError(@"%@ : kLARegistrationNewlettersIds required in plist info for manage newsletters",[LARegistrationManager class]);
    }
}

- (void)updateUserInfoWithNewsletterSubscription:(BOOL)shouldUpdateNewsSubscription andNewsletterUnsubscription:(BOOL)shouldUpdateNewsUnsubscription
{
    NSString *tokenOriginal = [[self.userClass sharedInstance]userToken];
    LARLog(@"Token original %@",tokenOriginal);
    
    NSString *tokenSha1 = [LARegistrationUtils sha1:tokenOriginal];
    LARLog(@"Token Sha1 %@",tokenSha1);
    
    NSString *ssoKeySha1 = [LARegistrationUtils sha1:kLARegistrationSSOKey];
    
    NSString *keyAndTokenOriginal = [NSString stringWithFormat:@"%@-%@",ssoKeySha1,tokenSha1];
    LARLog(@"keyAndTokenOriginal Sha1 %@",keyAndTokenOriginal);
    
    NSString *firstSha1KeyAndToken = [LARegistrationUtils sha1:keyAndTokenOriginal];
    LARLog(@"firstSha1KeyAndToken Sha1 %@",firstSha1KeyAndToken);
    
    NSString * logoutAppen = [NSString stringWithFormat:@"%@%@",kLARegistrationURLEditInfos,firstSha1KeyAndToken];
    NSMutableDictionary * params = [self dictionnaryToPostEditUser];
    
    NSError * error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:kHTTPPut URLString:logoutAppen   parameters:params error:&error];
    [request setValue: [NSString stringWithFormat:@"Basic %@", [self credentialFromLogin:kCredentialUser andPass:kCredentialPass]]  forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-type"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error == nil)
        {
           // NSDictionary * user = responseObject;
    
            LARLog(@"Update user Response Update %@", responseObject);
    
            if ([[responseObject objectForKey:kUserKey]objectForKey:kErrorKey])
            {
                NSString * key = [[responseObject objectForKey:kUserKey]objectForKey:kErrorKey];
    
                NSString * errorToDisplay = [[LARegistrationErrorHandler sharedInstance] getErrorWithKey:key];
                [KVNProgress showErrorWithStatus:errorToDisplay];
    
                TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                [dataLayer push:@{@"event": @"sendEvent",
                                  @"eventName":@"Compte",
                                  @"action": @"Erreur",
                                  @"section": @"",
                                  @"type": errorToDisplay,
                                  @"title":@""}];
            }
            else
            {
                [KVNProgress showSuccess];
                NSDictionary * userAnswer  = [responseObject objectForKey:kUserKey] ;
    
                TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                [dataLayer push:@{
                                  @"login":[userAnswer valueForKey:@"username"],
                                  @"email":[userAnswer valueForKey:@"email"],
                                  @"userid":[userAnswer valueForKey:@"id"]
                                  }];
    
                NSDictionary *userDetail=[userAnswer valueForKey:@"crm"];
    
                if(userDetail!=nil)
                {
                    [dataLayer push:@{
                                      @"firstName":[userDetail valueForKey:@"firstName"]==nil?@"":[userDetail valueForKey:@"firstName"],
                                      @"lastName":[userDetail valueForKey:@"lastName"]==nil?@"":[userDetail valueForKey:@"lastName"],
                                      @"gender":[userDetail valueForKey:@"gender"]==nil?@"":[userDetail valueForKey:@"gender"],
                                      @"birthdate":[userDetail valueForKey:@"birthdate"]==nil?@"":[userDetail valueForKey:@"birthdate"],
                                      @"zipcode":[userDetail valueForKey:@"zipcode"]==nil?@"":[userDetail valueForKey:@"zipcode"],
                                      @"city":[userDetail valueForKey:@"city"]==nil?@"":[userDetail valueForKey:@"city"],
                                      @"country":[userDetail valueForKey:@"country"]==nil?@"":[userDetail valueForKey:@"country"]
                                      }];
                }
    
                [dataLayer push:@{@"event": @"sendUserInfo"}];
                [dataLayer push:@{@"event": @"sendEvent",
                                  @"eventName":@"Compte",
                                  @"action": @"Edition",
                                  @"section": @"",
                                  @"type": @"",
                                  @"title":@""}];
                
                if(shouldUpdateNewsSubscription)
                {
                    [self updateNewslettersSubcription:[[self.userClass sharedInstance] newslettersSubscriptions] withType:kHTTPPost andShowSuccess:self.shouldShowNewsletterSubscriptionSuccess];
                }
                
                if(shouldUpdateNewsUnsubscription)
                {
                    [self updateNewslettersSubcription:[[self.userClass sharedInstance] newslettersUnsubscriptions] withType:kHTTPDelete  andShowSuccess:self.shouldShowNewsletterSubscriptionSuccess];
                }
            }

        }
        else
        {
                LARLogError(@" => Error: %@", [error localizedDescription]);
        
                [self catchAndDisplayError:responseObject];
                [KVNProgress showErrorWithStatus:@"Error"];
        
                TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
                [dataLayer push:@{@"event": @"sendEvent",
                                  @"eventName":@"Compte",
                                  @"action": @"Erreur",
                                  @"section": @"",
                                  @"type": error.description,
                                  @"title":@""}];
        }
    }];
    
    [dataTask resume];
    
}

// ==========================================
#pragma mark - Adapt datas for API Methods
// ==========================================

- (void)fillUserInfoWithGoogle:(GIDGoogleUser*)person
{
    [[self.userClass sharedInstance]setUserMail: person.profile.email];

    if (person.profile.name != NULL)
    {
        [[self.userClass sharedInstance]setUserPseudo:person.profile.name];
    }
}

- (void)fillUserInfoWithFacebook:(NSDictionary*)user
{
    if ([user objectForKey:@"email"] != NULL)
    {
        [[self.userClass sharedInstance]setUserMail:[user objectForKey:@"email"]];
    }
    
    if ([user objectForKey:@"name"] != NULL)
    {
        [[self.userClass sharedInstance]setUserPseudo:[user objectForKey:@"name"]];
    }
    
    if([[user objectForKey:@"gender"] isEqualToString:@"male"])
    {
        [[self.userClass sharedInstance]setUserSex:@"0"];
    }
    else if ([[user objectForKey:@"gender"] isEqualToString:@"female"])
    {
        [[self.userClass sharedInstance]setUserSex:@"1"];
    }
    else
    {
        [[self.userClass sharedInstance]setUserSex:@""];
    }
    
    //Adapt date in Euro format
    if ([user objectForKey:@"birthday"] != NULL)
    {
        NSString *dateString = [user objectForKey:@"birthday"];
        NSDateFormatter *parser = [[NSDateFormatter alloc] init];
        [parser setDateFormat:@"MM/dd/yyyy"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSString *dateStringEuFormat = [formatter stringFromDate:[parser dateFromString:dateString]];
        
        [[self.userClass sharedInstance]setUserBirthday:dateStringEuFormat];
    }
    
    if ([user objectForKey:@"first_name"] != NULL)
    {
        [[self.userClass sharedInstance]setUserPrenom:[user objectForKey:@"first_name"]];
    }
    
    if ([user objectForKey:@"last_name"] != NULL)
    {
        [[self.userClass sharedInstance]setUserNom:[user objectForKey:@"last_name"]];
    }
}

- (void)fillInfoFromApi:(NSDictionary*)user
{
    [[self.userClass sharedInstance]setUserMail:[user objectForKey:kEmailKey]];
    [[self.userClass sharedInstance]setUserPseudo:[user objectForKey:kUsernameKey]];
    [[self.userClass sharedInstance]setUserId:[[user objectForKey:kIdKey] stringValue]];
    [[self.userClass sharedInstance]setUserSex:[[user objectForKey:kCrmKey]objectForKey:kGenderKey]];
    [[self.userClass sharedInstance]setUserNom:[[user objectForKey:kCrmKey]objectForKey:kLastnameKey]];
    
    [[self.userClass sharedInstance]setUserPrenom:[[user objectForKey:kCrmKey]objectForKey:kFirstnameKey]];
    [[self.userClass sharedInstance]setUserAdress:[[user objectForKey:kCrmKey]objectForKey:kAddress1Key]];
    
    [[self.userClass sharedInstance]setUserCP:[[user objectForKey:kCrmKey]objectForKey:kZipCodeKey]];
    [[self.userClass sharedInstance]setUserCity:[[user objectForKey:kCrmKey]objectForKey:kCityKey]];
    [[self.userClass sharedInstance]setUserCountry:[[user objectForKey:kCrmKey]objectForKey:kCountryKey]];
    
    if ([user objectForKey:kDisplayUsernameKey])
    {
        [[self.userClass sharedInstance]setUserPseudo:[user objectForKey:kDisplayUsernameKey]];
    }
    
    //Adapt date in Euro format
    NSString *dateString = [[user objectForKey:kCrmKey]objectForKey:kBirthdayKey];
    [[self.userClass sharedInstance] setUserBirthday:[LARegistrationUtils adaptedDateForEu:dateString]];
    
    if ([[user objectForKey:kTokenKey]length])
    {
        [[self.userClass sharedInstance]setUserToken:[user objectForKey:kTokenKey]];
        [[NSUserDefaults standardUserDefaults]setObject:[user objectForKey:kTokenKey] forKey:kUserDefaultUserTokenKey];
    }
    else
    {
        [[self.userClass sharedInstance]setUserToken:[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserTokenKey]];
    }
    
    [[self.userClass sharedInstance] setNewslettersSubscriptions:nil];
    
    if(kLARegistrationNewlettersIds)
    {
        NSMutableArray * newsSubscriptions = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < [[[user objectForKey:kCrmKey]objectForKey:kNewslettersKey]count]; i++)
        {
            if ([[[[[user objectForKey:kCrmKey]objectForKey:kNewslettersKey]objectAtIndex:i]allKeys]containsObject:kIdKey])
            {
                for (NSString * newsletterId in kLARegistrationNewlettersIds)
                {
                    if([[[[[user objectForKey:kCrmKey]objectForKey:kNewslettersKey]objectAtIndex:i]valueForKey:kIdKey]integerValue] == [newsletterId integerValue] && ![newsSubscriptions containsObject:newsletterId])
                    {
                        [newsSubscriptions addObject:newsletterId];
                    }
                }
            }
        }
        
        [[self.userClass sharedInstance] setNewslettersSubscriptions:newsSubscriptions];
    }
    else
    {
         LARLogError(@"%@ : LARegistrationNewlettersIds required in plist info for manage newsletters",[LARegistrationManager class]);
    }
}

- (void)loadInfoFromUserDictionary:(NSDictionary*)userDictionary
{
    [self fillInfoFromApi:userDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLARegistrationNotificationUserLogged object:nil];
}

- (NSMutableDictionary*)dictionnaryToPostWithSocial
{
    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
    
    NSString *bornString = [[self.userClass sharedInstance]userBirthday];
    
    if ([bornString length])
    {
        NSString *day = @"";
        long dayInt;
        NSString *month = @"";
        long monthInt;
        NSString *year = @"";
        long yearInt;
        
        NSArray * bornExtract = [bornString componentsSeparatedByString:@"/"];
        
        day = [bornExtract objectAtIndex:0];
        dayInt = [day integerValue];
        day = [NSString stringWithFormat:@"%ld",dayInt];
        
        month = [bornExtract objectAtIndex:1];
        monthInt = [month integerValue];
        month = [NSString stringWithFormat:@"%ld",monthInt];
        
        year = [bornExtract objectAtIndex:2];
        yearInt = [year integerValue];
        year = [NSString stringWithFormat:@"%ld",yearInt];
        
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][birthdate][day]":day,
                                           @"la_user_api_social[crm][birthdate][month]":month,
                                           @"la_user_api_social[crm][birthdate][year]":year
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userMail] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[email]": [[self.userClass sharedInstance]userMail]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userPrenom] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][firstName]":[[self.userClass sharedInstance]userPrenom]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userNom] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][lastName]":[[self.userClass sharedInstance]userNom]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userSex]integerValue] != 2)
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][gender]":[[self.userClass sharedInstance]userSex]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userAdress] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][address1]":[[self.userClass sharedInstance]userAdress]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userCP] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][zipcode]":[[self.userClass sharedInstance]userCP]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userCity] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][city]":[[self.userClass sharedInstance]userCity]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userCountry] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_social[crm][country]":[[self.userClass sharedInstance]userCountry]
                                           }];
    }
    
    return  params;
}

- (NSMutableDictionary*)dictionnaryToCreateNewUser
{
    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[username]": [[self.userClass sharedInstance]userPseudo]
                                       }];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[crmTmp][firstName]": [[self.userClass sharedInstance]userPrenom]
                                       }];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[crmTmp][lastName]": [[self.userClass sharedInstance]userNom]
                                       }];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[crmTmp][gender]": [[self.userClass sharedInstance]userSex]
                                       }];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[email]": [[self.userClass sharedInstance]userMail]
                                       }];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[plainPassword][first]":[[self.userClass sharedInstance]userPass]
                                       }];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[plainPassword][second]":[[self.userClass sharedInstance]userPass]
                                       }];
    
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[crmTmp][zipcode]":[[self.userClass sharedInstance]userCP]
                                       }];
    
    if ([[[self.userClass sharedInstance]userAdress] length]){
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_registration[crmTmp][address1]": [[self.userClass sharedInstance]userAdress]
                                           }];
    }
    if ([[[self.userClass sharedInstance]userCity] length]){
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_registration[crmTmp][city]": [[self.userClass sharedInstance]userCity]
                                           }];
    }

    
    
    if ([[[self.userClass sharedInstance]userCountry] length])
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_registration[crmTmp][country]":[[self.userClass sharedInstance]userCountry]
                                           }];
    else
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_registration[crmTmp][country]":@"FR"
                                           }];
    }
    
    NSString *bornString = [[self.userClass sharedInstance]userBirthday];
    
    if ([bornString length])
    {
        NSString *day = @"";
        long dayInt;
        NSString *month = @"";
        long monthInt;
        NSString *year = @"";
        long yearInt;
        
        NSArray * bornExtract = [bornString componentsSeparatedByString:@"/"];
        
        day = [bornExtract objectAtIndex:0];
        dayInt = [day integerValue];
        day = [NSString stringWithFormat:@"%ld",dayInt];
        
        month = [bornExtract objectAtIndex:1];
        monthInt = [month integerValue];
        month = [NSString stringWithFormat:@"%ld",monthInt];
        
        year = [bornExtract objectAtIndex:2];
        yearInt = [year integerValue];
        year = [NSString stringWithFormat:@"%ld",yearInt];
        
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_registration[crmTmp][birthdate][day]":day,
                                           @"la_user_api_registration[crmTmp][birthdate][month]":month,
                                           @"la_user_api_registration[crmTmp][birthdate][year]":year
                                           }];
    }
   
    return params;
}

- (NSMutableDictionary*) dictionnaryToPostEditUser
{   
    NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
    
    NSString *bornString = [[self.userClass sharedInstance]userBirthday];
    
    if ([bornString length])
    {
        NSString *day = @"";
        long dayInt;
        NSString *month = @"";
        long monthInt;
        NSString *year = @"";
        long yearInt;
        
        NSArray * bornExtract = [bornString componentsSeparatedByString:@"/"];
        
        day = [bornExtract objectAtIndex:0];
        dayInt = [day integerValue];
        day = [NSString stringWithFormat:@"%ld",dayInt];
        
        month = [bornExtract objectAtIndex:1];
        monthInt = [month integerValue];
        month = [NSString stringWithFormat:@"%ld",monthInt];
        
        year = [bornExtract objectAtIndex:2];
        yearInt = [year integerValue];
        year = [NSString stringWithFormat:@"%ld",yearInt];
        
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][birthdate][day]":day,
                                           @"la_user_api_profile[crm][birthdate][month]":month,
                                           @"la_user_api_profile[crm][birthdate][year]":year
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userPrenom] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][firstName]":[[self.userClass sharedInstance]userPrenom]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userNom] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][lastName]":[[self.userClass sharedInstance]userNom]
                                           }];
    }

    LARLog(@"Sex user is %@",[[self.userClass sharedInstance]userSex] );
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_profile[crm][gender]":[[self.userClass sharedInstance]userSex]
                                       }];
    
    if ([[[self.userClass sharedInstance]userAdress] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][address1]":[[self.userClass sharedInstance]userAdress]
                                           }];
    }

    if ([[[self.userClass sharedInstance]userCP]length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][zipcode]":[[self.userClass sharedInstance]userCP]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userCity] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][city]":[[self.userClass sharedInstance]userCity]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userCountry] length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][country]":[[self.userClass sharedInstance]userCountry]
                                           }];
    }
    
    if ([[[self.userClass sharedInstance]userAdress]length])
    {
        [params addEntriesFromDictionary:@{
                                           @"la_user_api_profile[crm][address1]":[[self.userClass sharedInstance]userAdress]
                                           }];
    }
    
    return params;
}

- (void)resetAutoConnectDatas
{
    //Re-init des variables d'auto-connect
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserDefaultUserTokenKey];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserDefaultUserCredentialKey];
    
    [[self.userClass sharedInstance] setUserToken:@""];
}

- (void) resetStoredDatas
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserDefaultUserTokenKey];
    [[self.userClass sharedInstance] reinitFields];
}

- (void)catchAndDisplayError:(id)response
{
    @try
    {
        //NSString *response =  [[NSString alloc] initWithData:[operation responseData] encoding:NSUTF8StringEncoding];
        id json;
        
        if([response isKindOfClass:[NSDictionary class]])
        {
            json = response;
        }
        else
        {
            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
       
        NSDictionary * userAnswer  = [json objectForKey:kUserKey] ;
        
        //LARLog(@"Error Message Reponse %@", [json description]);
        
        if ([[userAnswer objectForKey:kErrorKey] isEqualToString:kInvalidTokenKey])
        {
            [self resetStoredDatas];
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserCredentialKey]length])
            {
                [self loginByUsingCredentials:[[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultUserCredentialKey] withSuccess:^(id response) {
                    
                } failure:^(id errors) {
                    
                }];
                
                return;
            }
            else
            {
                LARLog(@"User disconnected");
            }
        }
        
        if ([[userAnswer objectForKey:kErrorKey] isEqualToString:kValidationFailedKey])
        {
            NSString * errosString = @"";
            NSDictionary * form = [userAnswer objectForKey:kFormKey] ;
            
            if ([[form allKeys]containsObject:kCrmKey])
            {
                form = [[userAnswer objectForKey:kFormKey]objectForKey:kCrmKey];
            }
            
            for (int i = 0; i < [[form allKeys]count]; i++)
            {
                if ([[[form allKeys]objectAtIndex:i]isEqualToString:kPlainPasswordKey])
                {
                    errosString = [[[form objectForKey:kPlainPasswordKey] objectForKey:kFirstKey] objectForKey:kErrorMessageKey];
                    errosString = [errosString stringByAppendingString:@"\n"];
                }
                else if([[[form objectForKey:[[form allKeys]objectAtIndex:i]]objectForKey:kErrorMessageKey]length])
                {
                    LARLog(@"Error for %@", [[form objectForKey:[[form allKeys]objectAtIndex:i]]objectForKey:kErrorMessageKey]);
                    errosString = [errosString stringByAppendingFormat:@"%@\n", [[form objectForKey:[[form allKeys]objectAtIndex:i]]objectForKey:kErrorMessageKey]];
                }
            }
            
            //On supprime le prefix [LA] devant les string d'erreur retournées
            errosString = [errosString stringByReplacingOccurrencesOfString:@"[LA]" withString:@""];
            
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Erreur" message:errosString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            NSString * errorToDisplay = [[LARegistrationErrorHandler sharedInstance]getErrorWithKey:[userAnswer objectForKey:kErrorKey]];
            //[self resetAutoConnectDatas]; // Do not reset info because the error is may be due to the fact there is not internet connection or a timeout
            if (errorToDisplay.length > 0)
            {
                [KVNProgress showErrorWithStatus:errorToDisplay];
            }
            else /*if (operation.error.localizedDescription.length > 0)*/
            {
                //[KVNProgress showErrorWithStatus:operation.error.localizedDescription];
                [KVNProgress showErrorWithStatus:[[LARegistrationErrorHandler sharedInstance] getErrorWithKey:@"unkwown_error"]];
            }
            
            LARLog(@"Erreur Catch API error reason is  %@", [userAnswer objectForKey:kErrorKey]);
        }
    }
    @catch (NSException *exception)
    {
        LARLogException(@"Exception in catch error LARegistrationManager : %@",[exception reason]);
    }
}

// ==========================================
#pragma mark - Social Login Methods
#pragma mark -
// ==========================================

// Build Facebook Social login Deeplink signature according app parameters
- (NSString *)getFbLoginDeeplink
{
    NSString *fbLoginDeepLinkScheme = nil;
    
    if ([[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"] != nil)
    {
        fbLoginDeepLinkScheme = [NSString stringWithFormat:@"fb%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"]];
    }
    
    return fbLoginDeepLinkScheme;
}

// Build Google Sign In login Deeplink signature according app parameters
- (NSString *)getGoogleSignInDeeplink
{
    NSString *gSignInReversedCliendId = nil;

    if (kGPPClientId != nil)
    {
        // The app deeplink scheme is the reverse og the client ID
        gSignInReversedCliendId = nil;
        NSArray* clientIdArray = [kGPPClientId  componentsSeparatedByString:@"."];
        NSArray *reverseClientIdArray = [[clientIdArray reverseObjectEnumerator] allObjects];
        gSignInReversedCliendId = [[reverseClientIdArray valueForKey:@"description"] componentsJoinedByString:@"."];
    }
    
    return gSignInReversedCliendId;
}

// Check is URL is a callback (deeplink) url for a social sign-in
- (BOOL)isSocialSignInCallbackFor:(NSURL *)aCallbackUrl
{
    if ( ([self getFbLoginDeeplink] != nil) && ([[aCallbackUrl scheme] isEqualToString:[self getFbLoginDeeplink]] == TRUE) )
    {
        return TRUE;
    }
    else if ( ([self getGoogleSignInDeeplink] != nil) && ([[aCallbackUrl scheme] isEqualToString:[self getGoogleSignInDeeplink]] == TRUE) )
    {
        return TRUE;
    }
    
    return FALSE; // default return
}

// Try to make social login according  url callback else return FALSE
- (BOOL)socialLoginWith:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    // Check it's a social Login callback url
    if ([self isSocialSignInCallbackFor:url] == TRUE)
    {
        if ( ([self getFbLoginDeeplink] != nil) && ([[url scheme] isEqualToString:[self getFbLoginDeeplink]] == TRUE) )
        {
            return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        }
        else if ( ([self getGoogleSignInDeeplink] != nil) && ([[url scheme] isEqualToString:[self getGoogleSignInDeeplink]] == TRUE) )
        {
            return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
        }
    }
    
    return FALSE; //  by default return
}

// ==========================================
#pragma mark - Facebook login Methods
// ==========================================

- (void)authenticateWithFacebookFromVC:(UIViewController*)controller
{
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"network":@"Facebook"}];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    // Check if we are on iOS < 9 And FaceBook app is install
    if ( ([[[UIDevice currentDevice] systemVersion] compare:@"9" options:NSNumericSearch] == NSOrderedDescending) && ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) )
    {
        // Made facebook login throught app switch
        [login setLoginBehavior:FBSDKLoginBehaviorNative];
    }
    else
    {
        // Made Facebook login throught thier moald webview
        [login setLoginBehavior:FBSDKLoginBehaviorWeb];
    }
    
    [login setLoginBehavior:FBSDKLoginBehaviorNative];
    
    [login  logInWithReadPermissions: @[@"public_profile", @"email",@"user_birthday"]
                  fromViewController:controller
                             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                 
                                 if (error)
                                 {
                                     LARLogError(@"Process error =>  description : %@",[error localizedDescription]);
                                     
                                     FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                     [loginManager logOut];
                                     
                                     [dataLayer push:@{@"event": @"sendEvent",
                                                       @"eventName":@"Compte",
                                                       @"action": @"Erreur",
                                                       @"section": @"",
                                                       @"type": error.userInfo.description,
                                                       @"title":@""}];
                                     
                                     [KVNProgress dismiss];
                                     [self forceKVNDismiss];
                                     
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Echec de la connexion via facebook"
                                                                                     message:@""
                                                                                    delegate:nil
                                                                           cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                     [alert show];
                                     
                                 }
                                 else if (result.isCancelled)
                                 {
                                     LARLog(@" FB login Cancelled by user");
                                     
                                     [dataLayer push:@{@"event": @"sendEvent",
                                                       @"eventName":@"Compte",
                                                       @"action": @"Annulation",
                                                       @"section": @"",
                                                       @"type": @"",
                                                       @"title":@""}];
                                     
                                     [KVNProgress dismiss];
                                     [self forceKVNDismiss];
                                 }
                                 else
                                 {
                                     LARLog(@"Logged in");
                                     
                                     if ([FBSDKAccessToken currentAccessToken])
                                     {
                                         [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email , name, birthday, gender, first_name, last_name, location"}]
                                          startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id user, NSError *error2)
                                          {
                                              if (error2 == nil)
                                              {
                                                  [[NSUserDefaults standardUserDefaults]setObject:@"facebook" forKey:@"originUser"];
                                                  
                                                  NSString *fbAccessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
                                                  
                                                  [self fillUserInfoWithFacebook:(NSDictionary*)user];
                                                  
                                                  [KVNProgress showWithStatus:@"Connexion en cours..."];
                                                  
                                                  [self logIntoApiWithSocialNetworkWithKey:@"facebookid" Value:[user objectForKey:@"id"] andTokenName:@"facebook_access_token" Value:fbAccessToken withSuccess:nil failure:nil];
                                              }
                                              else
                                              {
                                                  [dataLayer push:@{@"event": @"sendEvent",
                                                                    @"eventName":@"Compte",
                                                                    @"action": @"Erreur",
                                                                    @"section": @"",
                                                                    @"type": error2.userInfo.description,
                                                                    @"title":@""}];
                                                  
                                                  FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                                  [loginManager logOut];
                                                  [KVNProgress dismiss];
                                                  [self forceKVNDismiss];
                                              }
                                          }];
                                     }
                                 }
                             }];
}

// ==========================================
#pragma mark - Google SignIn Methods
// ==========================================

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    [self authenticateWithGoogle:signIn user:user withError:error];
}

- (void)authenticateWithGoogle:(GIDSignIn*)signIn user:(GIDGoogleUser*)user withError:(NSError*)error
{
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"network":@"Google"}];
    
    if (error)
    {
        if(error.code==kGIDSignInErrorCodeCanceled)
        {
            [dataLayer push:@{@"event": @"sendEvent",
                              @"eventName":@"Compte",
                              @"action": @"Annulation",
                              @"section": @"",
                              @"type": @"",
                              @"title":@""}];
        }
        else
        {
            [dataLayer push:@{@"event": @"sendEvent",
                              @"eventName":@"Compte",
                              @"action": @"Erreur",
                              @"section": @"",
                              @"type": error.userInfo.description,
                              @"title":@""}];
        }

        LARLog(@"Error Google+ connect : %@",[error localizedDescription]);
        
        [KVNProgress dismiss];
        [self forceKVNDismiss];
        
        if (error.code != kGIDSignInErrorCodeCanceled)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Echec de la connexion via Google Sign in"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
        
        return;
    }
    
    GIDGoogleUser *person = [GIDSignIn sharedInstance].currentUser;
    
    if (person == nil)
    {
        [dataLayer push:@{@"event": @"sendEvent",
                          @"eventName":@"Compte",
                          @"action": @"Erreur",
                          @"section": @"person nil",
                          @"type": @"",
                          @"title":@""}];
        
        if (error.code != kGIDSignInErrorCodeCanceled)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Echec de la connexion via Google Sign in"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
        return;
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:@"google" forKey:@"originUser"];
    
    LARLog(@"person is %@ auth is %@ person description %@",person.userID,user,[person description]);
    
    [self fillUserInfoWithGoogle:person];
    
    [KVNProgress showWithStatus:@"Connexion en cours..."];
    [self logIntoApiWithSocialNetworkWithKey:@"googleid" Value:person.userID andTokenName:@"google_access_token" Value:user.authentication.accessToken withSuccess:nil failure:nil];
}

// ==========================================
#pragma mark - Convenient Methods
// ==========================================

- (void)forceKVNDismiss
{
    UIWindow * window = [[[UIApplication sharedApplication] delegate] window];
    
    for (UIView * view in window.subviews)
    {
        if([view isKindOfClass:[KVNProgress class]])
        {
            [view removeFromSuperview];
        }
    }
}

- (void)debugLogStatus
{
    if (self.debugLogEnable)
    {
        NSLog(@"[LARegistration debugLogEnable] is Active");
    }
    else
    {
        NSLog(@"[LARegistration debugLogEnable] is Disable");
    }
    
    LARLog(@"This is an information Log");
    LARLogError(@"This is an ERROR Log !!!");
    LARLogException(@"This is an Exception Error Log");
}

@end
