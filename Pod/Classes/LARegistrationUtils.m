//
//  LARegistrationUtils.m
//  LARegistrationPOC
//
//  Created by BELLEROSE Odile on 19/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "LARegistrationUtils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation LARegistrationUtils

//
+ (NSString*)adaptedDateForEu:(NSString*)usDate
{
    NSString *dateString = usDate;
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T00:00:00+0200" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T00:00:00+0100" withString:@""];
    NSDateFormatter *parser = [[NSDateFormatter alloc] init];
    [parser setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *dateStringEuFormat = [formatter stringFromDate:[parser dateFromString:dateString]];
    
    return dateStringEuFormat;
}

+ (NSString *)base64Encode:(NSString *)plainText
{
    NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainTextData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    return base64String;
}

+ (NSString*)sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (int)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

+ (BOOL)mailIsCorrect:(NSString*)mail
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:mail] == NO)
    {
        return FALSE;
    }
    
    return TRUE;
}

@end
