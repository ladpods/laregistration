# LARegistration

<!--
[![CI Status](http://img.shields.io/travis/odile bellerose/LARegistration.svg?style=flat)](https://travis-ci.org/odile bellerose/LARegistration) 
[![Version](https://img.shields.io/cocoapods/v/LARegistration.svg?style=flat)](http://cocoapods.org/pods/LARegistration)/
[![License](https://img.shields.io/cocoapods/l/LARegistration.svg?style=flat)](http://cocoapods.org/pods/LARegistration)
[![Platform](https://img.shields.io/cocoapods/p/LARegistration.svg?style=flat)](http://cocoapods.org/pods/LARegistration) -->

## Author



Lagardere Active 

odile bellerose, extia-odile.bellerose@lagardere-active.com
Yohan Texeira,yohan.teixeira@lagardere-active.com
Yann Dupuy , yann.dupuy@lagardere-active.com
LAD iOS Team, ios-dev@lagardere-active.com

---------


## License

LARegistration is available under the MIT license. See the LICENSE file for more info.

----------

## Description

LARegistration is an iOS framework for user login/registration through LARegistration API and Social login (Facebook / Google Sign-In)

## Installation

LARegistration is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LARegistration"
```

-------
## Requis - Configuration

Ajouter les propriétés suivantes dans le fichier info.plist :

#### LARegistration API Parameters ####

* LARegistrationCredentialPass => String
* LARegistrationCredentialUser => String
* LARegistrationSSOKey => String
* LARegistrationSiteId => String
* LARegistrationURLGetInfo => String
* LARegistrationURLLogin => String
* LARegistrationURLLogout => String
* LARegistrationURLSocialLogin => String
* LARegistrationURLRegister => String
* LARegistrationURLNewsletter => String
* LARegistrationURLEditInfos => String
* LARegistrationURLForgotPass => String
* LARegistrationNewlettersIds => Array

#### Google SignIn SDK Parameters ####

* GPPIdClient => String
* Add in "URL Schemes" callback url for google SignIn (reverse client ID)

#### FBSDKLoginKit SDK Parameters ####

* FacebookAppID => string
* FacebookDisplayName
* Add in "URL Schemes" callback url for facebook login ( "fbYOUR_APP_ID")

-------
# Implementation


@see LARegistration-Example


-------
# Customisation

-------


## Exemple d'ajout de propriétés


Pour ajouter des propriétés aux infos utilisateurs, créer une classe héritant de LARegistrationUserInfos, ex : PMRegistrationUserInfos.

Dans le .h  :

```
@interface PMRegistrationUserInfos : LARegistrationUserInfos
@property (nonatomic, assign)   BOOL  subscribeStatus;
+ (PMRegistrationUserInfos*)sharedInstance;
@end
```
Dans le .m  (methods to override / extends) :

```
@implementation PMRegistrationUserInfos

+ (PMRegistrationUserInfos *)sharedInstance
{
	static PMRegistrationUserInfos *sharedInstance = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		sharedInstance = [[PMRegistrationUserInfos alloc] init];
	});
	
	return sharedInstance;
}

- (void)reinitFields
{
	[super reinitFields];

	self.subscribeStatus = NO;
}

- (void)backupUserInfoDatas
{
    [super backupUserInfoDatas];

    [self.userInfoBackup setValue:[NSNumber numberWithBool:self.subscribeStatus] forKey:@"subscribeStatus"];
}


- (void)resetDatasWithBackup
{
    [super resetDatasWithBackup];
    
    self.subscribeStatus = [[self.userInfoBackup objectForKey@"subscribeStatus"] boolValue];
}

- (void)debugAllFields
{
	[super debugAllFields];
	NSLog(@"subscribeStatus : %d",self.subscribeStatus);
}

```

Il faut également créer une classe qui hérite de LARegistrationManager, ex: PMRegistrationManager.
Dans le .h ajouter :

```
+ (PMRegistrationManager*) sharedInstance;

```
Dans le .m :

```
+ (PMRegistrationManager *)sharedInstance
{
	static PMRegistrationManager *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	
		sharedInstance = [[PMRegistrationManager alloc] init];
		
	});
	
	return sharedInstance;
}

- (NSMutableDictionary*)dictionnaryToCreateNewUser
{
	NSMutableDictionary * params = [super dictionnaryToCreateNewUser];
	[params addEntriesFromDictionary:@{
		@"la_user_api_registration[crmTmp][subscribeStatus]": 		@([[PMRegistrationUserInfos sharedInstance] subscribeStatus])
	}];

	[params addEntriesFromDictionary:@{@"la_user_api_registration[crmTmp][gcu]" : @1
}];

	return params;
}

- (NSMutableDictionary*)dictionnaryToPostEditUser
{
	NSMutableDictionary * params = [super dictionnaryToPostEditUser];
	[params addEntriesFromDictionary:@{
	@"la_user_api_registration[crm][subscribeStatus]": @([[PMRegistrationUserInfos sharedInstance] subscribeStatus])
	}];
	
	return params;
}

```

Dans le didFinishLaunchingWithOptions de l'AppDelegate ajouter les lignes suivantes pour indiquer que vous n'utilisez pas la classe par défaut :

```
[[LARegistrationManager sharedInstance] setUserClass:[PMRegistrationUserInfos class]];
[[PMRegistrationManager sharedInstance] setUserClass:[PMRegistrationUserInfos class]];
```

Note : pour toutes proprietes aujouter dans une classe heritant de `LARegistrationUserInfos` il faut egalement redefinir les methodes de serialisation afin de permettre l'archivage du profile user


```
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
    // your new propertities decoding method
        self.subscribeStatus = [[aDecoder decodeObjectForKey:@"subscribeStatus"] boolValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    
    // your new propertities encoding method
    [aCoder encodeObject:@(self.subscribeStatus) forKey:@"subscribeStatus"];
}
```


