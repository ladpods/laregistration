//
//  main.m
//  LARegistration
//
//  Created by odile bellerose on 11/24/2015.
//  Copyright (c) 2015 odile bellerose. All rights reserved.
//

@import UIKit;
#import "LARegistrationAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LARegistrationAppDelegate class]));
    }
}
