//
//  POCUserInfos.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 23/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "LARegistrationUserInfos.h"

@interface POCUserInfos : LARegistrationUserInfos

/***  PROPERTIES  ***/

@property (nonatomic, assign)   BOOL  subscribeStatus;

/***  METHODS  ***/

+ (POCUserInfos*)sharedInstance;

@end
