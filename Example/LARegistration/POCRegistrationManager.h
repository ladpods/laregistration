//
//  POCRegistrationManager.h
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 23/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "LARegistrationManager.h"

@interface POCRegistrationManager : LARegistrationManager

/***  METHODS  ***/

+ (POCRegistrationManager*) sharedInstance;

@end
