//
//  LARegistrationViewController.m
//  LARegistration
//
//  Created by odile bellerose on 11/24/2015.
//  Copyright (c) 2015 odile bellerose. All rights reserved.
//

#import "LARegistrationViewController.h"

#import "POCRegistrationManager.h"
#import "LARegistrationConstant.h"
#import "LARegistrationErrorHandler.h"
#import "POCUserInfos.h"
#import <KVNProgress/KVNProgress.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LARegistrationViewController ()<GIDSignInDelegate, GIDSignInUIDelegate>

@end

@implementation LARegistrationViewController

#pragma mark - View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    GIDSignIn* signIn = [GIDSignIn sharedInstance];

    signIn.clientID = kGPPClientId;
    signIn.scopes = @[ @"profile", @"email" ];
    signIn.delegate = self;
    signIn.uiDelegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationUserLogged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationUserLogout object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationErrorCreateUser object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationUpdateIsDone object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationDidChangeMail object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationDidChangePass object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kLARegistrationNotificationDidSendForgottedPass object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Action Methods

- (IBAction)didTouchLoginButton:(id)sender
{
    if([[[POCUserInfos sharedInstance] userToken] length])
    {
        [[POCRegistrationManager sharedInstance] logout];
    }
    else
    {
        //NSString * credentials = [[POCRegistrationManager sharedInstance] credentialFromLogin:@"flo39510@clipmail.eu" andPass:@"123456"];
        NSString * credentials = [[POCRegistrationManager sharedInstance] credentialFromLogin:@"ios-dev@lagardere-active.com" andPass:@"ln21egal0"];
        [[POCRegistrationManager sharedInstance] loginByUsingCredentials:credentials withSuccess:nil failure:nil];
    }
}

- (IBAction)didTouchGoogleButton:(id)sender
{
     [[GIDSignIn sharedInstance] signIn];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    // Perform any operations on signed in user here.
    [[LARegistrationManager sharedInstance] authenticateWithGoogle:signIn user:user withError:error];
}

- (IBAction)didTouchFacebookButton:(id)sender
{
    [[POCRegistrationManager sharedInstance] authenticateWithFacebookFromVC:self];
}

- (IBAction)didTouchRegisterButton:(id)sender
{
    [[POCUserInfos sharedInstance] setUserPseudo:@"LAD iOS Dev"];
    [[POCUserInfos sharedInstance] setUserPrenom:@"Test"];
    [[POCUserInfos sharedInstance] setUserNom:@"LAD iOS Dev"];
    [[POCUserInfos sharedInstance] setUserSex:@"0"];
    [[POCUserInfos sharedInstance] setUserMail:@"ios-dev@lagardere-active.com"];
    [[POCUserInfos sharedInstance] setUserPass:@"1234567*"];
    [[POCUserInfos sharedInstance] setUserCP:@"95140"];
    [[POCUserInfos sharedInstance] setUserAdress:@"24 rue paul gauguin"];
    [[POCUserInfos sharedInstance] setUserCity:@"Paris"];
    [[POCUserInfos sharedInstance] setUserCountry:@"FR"];
    [[POCUserInfos sharedInstance] setUserBirthday:@"15/10/1990"];
    [[POCUserInfos sharedInstance] addNewsletterWithId:@"1"];
    
    [[POCRegistrationManager sharedInstance] registerNewUser];
}

- (IBAction)didTouchEditButton:(id)sender
{
    [[POCUserInfos sharedInstance] setNewslettersSubscriptions:[[NSMutableArray alloc] initWithArray:@[@"2",@"3"]]];
    [[POCRegistrationManager sharedInstance] updateUserInfoWithNewsletterSubscription:YES andNewsletterUnsubscription:YES];
}

- (IBAction)didTouchChangePassButton:(id)sender
{
    [[POCRegistrationManager sharedInstance] changePass:@"[PREVIOUS_PASS]" andNewPass:@"[NEW_PASS]"];
}

- (IBAction)didTouchChangeEmailButton:(id)sender
{
    [[POCRegistrationManager sharedInstance] changeMail:@"[NEW_MAIL]"];
}

- (IBAction)didTouchForgotPassButton:(id)sender
{
    [[POCRegistrationManager sharedInstance] forgotPassWithMailAdress:@"[YOUR_MAIL]"];
}

- (void)didReceiveNotification:(NSNotification*) notification
{
    if([notification.name isEqualToString:kLARegistrationNotificationUserLogged] || [notification.name isEqualToString:kLARegistrationNotificationUpdateIsDone])
    {
        [self.loginButton setTitle:@"Logout" forState:UIControlStateNormal];
        [self.editButton setHidden:NO];
        [self.changeEmailButton setHidden:NO];
        [self.changePassButton setHidden:NO];
        [self.forgotPassButton setHidden:YES];
        [self.registrationButton setHidden:YES];
    }
    
    if([notification.name isEqualToString:kLARegistrationNotificationUserLogout])
    {
        [self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
        [self.editButton setHidden:YES];
        [self.changeEmailButton setHidden:YES];
        [self.changePassButton setHidden:YES];
        [self.forgotPassButton setHidden:NO];
        [self.registrationButton setHidden:NO];
    }
    
    if([notification.name isEqualToString:kLARegistrationNotificationErrorCreateUser])
    {
        if(notification.object)
        {
        }
    }
    
    if([notification.name isEqualToString:kLARegistrationNotificationDidChangeMail])
    {
    }
    
    if([notification.name isEqualToString:kLARegistrationNotificationDidChangePass])
    {
    }
    
    if([notification.name isEqualToString:kLARegistrationNotificationDidSendForgottedPass])
    {
    }
}

#pragma mark - Memory Methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
