//
//  POCUserInfos.m
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 23/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "POCUserInfos.h"

@implementation POCUserInfos

+ (POCUserInfos *)sharedInstance
{
    static POCUserInfos *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[POCUserInfos alloc] init];
    });
    
    return sharedInstance;
}

- (void)reinitFields
{
    [super reinitFields];
    self.subscribeStatus = NO;
}

- (void)debugAllFields
{
    [super debugAllFields];
    
    NSLog(@"subscribeStatus : %d",self.subscribeStatus);
}

@end
