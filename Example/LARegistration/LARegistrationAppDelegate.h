//
//  LARegistrationAppDelegate.h
//  LARegistration
//
//  Created by odile bellerose on 11/24/2015.
//  Copyright (c) 2015 odile bellerose. All rights reserved.
//

@import UIKit;

@interface LARegistrationAppDelegate : UIResponder <UIApplicationDelegate>

/***  PROPERTIES  ***/

@property (strong, nonatomic) UIWindow *window;

@end
