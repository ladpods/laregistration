//
//  POCRegistrationManager.m
//  LARegistration - CocoaPod
//
//  Created by BELLEROSE Odile on 23/11/2015.
//  Copyright © 2015-2016 Lagardere Active. All rights reserved.
//

#import "POCRegistrationManager.h"

#import "POCUserInfos.h"

@implementation POCRegistrationManager

+ (POCRegistrationManager *)sharedInstance
{
    static POCRegistrationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedInstance = [[POCRegistrationManager alloc] init];
    });
    
    return sharedInstance;
}

- (NSMutableDictionary*) dictionnaryToCreateNewUser
{
    NSMutableDictionary * params = [super dictionnaryToCreateNewUser];
 
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[crmTmp][subscribeStatus]": @([[POCUserInfos sharedInstance] subscribeStatus])
                                       }];
    
    [params addEntriesFromDictionary:@{@"la_user_api_registration[crmTmp][gcu]" : @1
                                               }];
    return params;
}

- (NSMutableDictionary*) dictionnaryToPostEditUser
{
    NSMutableDictionary * params = [super dictionnaryToPostEditUser];
    
    [params addEntriesFromDictionary:@{
                                       @"la_user_api_registration[crm][subscribeStatus]": @([[POCUserInfos sharedInstance] subscribeStatus])
                                       }];
    return params;
}

@end
