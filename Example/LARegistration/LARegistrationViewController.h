//
//  LARegistrationViewController.h
//  LARegistration
//
//  Created by odile bellerose on 11/24/2015.
//  Copyright (c) 2015 odile bellerose. All rights reserved.
//

@import UIKit;
#import <GoogleSignIn/GoogleSignIn.h>

@interface LARegistrationViewController : UIViewController

/***  PROPERTIES  ***/

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *fbLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *changePassButton;
@property (weak, nonatomic) IBOutlet UIButton *changeEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *registrationButton;
@property (weak, nonatomic) IBOutlet GIDSignInButton *ggLoginButton;

@end
