//
//  LARegistrationAppDelegate.m
//  LARegistration
//
//  Created by odile bellerose on 11/24/2015.
//  Copyright (c) 2015 odile bellerose. All rights reserved.
//

#import "LARegistrationAppDelegate.h"

#import "POCRegistrationManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "KVNProgress.h"

#import "LARegistrationConstant.h"
#import "POCUserInfos.h"

// ================================================================================================================================

NSString *const kNewsletterId       = @"1";
NSString *const kOffrePartenairesId = @"2";
NSString *const kNewsletterClubId   = @"3";

// ================================================================================================================================

@implementation LARegistrationAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [[LARegistrationManager sharedInstance] setDebugLogEnable:TRUE];
    [[LARegistrationManager sharedInstance] debugLogStatus];
    
    [[POCRegistrationManager sharedInstance] checkAutoConnect];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    [[LARegistrationManager sharedInstance] setUserClass:[POCUserInfos class]];
    [[POCRegistrationManager sharedInstance] setUserClass:[POCUserInfos class]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// For iOS 9.0 and later
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options
{
    if ([[LARegistrationManager sharedInstance] isSocialSignInCallbackFor:url] == TRUE)
    {
        return [[LARegistrationManager sharedInstance] socialLoginWith:app
                                                               openURL:url
                                                     sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                            annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    else
    {
        // Put any other deeplink routing
    }
    
    return FALSE;
}

// For iOS before 9.0
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[LARegistrationManager sharedInstance] isSocialSignInCallbackFor:url] == TRUE)
    {
        return [[LARegistrationManager sharedInstance] socialLoginWith:application
                                                               openURL:url
                                                     sourceApplication:sourceApplication
                                                            annotation:annotation];
    }
    else
    {
        // Put any other deeplink routing
    }
    
    return FALSE;
}

@end
